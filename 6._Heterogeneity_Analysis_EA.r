setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel")


# Overlap across ancestry groups
leadsnps = list()
leadsnps[["EA"]] = list()
leadsnps[["AA"]] = list()
files = dir("cojo_files", recursive=T)[grep("_EA/*.*.jma.merge$", dir("cojo_files",recursive=T))]
for(file in files){
	leadsnps[["EA"]][[file]] = read.table(paste0("cojo_files/", file), header=T)
	leadsnps[["EA"]][[file]]$MarkerName = paste0(leadsnps[["EA"]][[file]]$Chr, ":", leadsnps[["EA"]][[file]]$bp)
}
files = dir("cojo_files", recursive=T)[grep("ARIC/gcta_out_5e-6_extract/*.*5e-8.jma.merge$", dir("cojo_files",recursive=T))]
for(file in files){
	leadsnps[["AA"]][[file]] = read.table(paste0("cojo_files/", file), header=T)
	leadsnps[["AA"]][[file]]$MarkerName = paste0(leadsnps[["AA"]][[file]]$Chr, ":", leadsnps[["AA"]][[file]]$bp)
}

for(i in 1:length(leadsnps[["EA"]])){
	write.table(leadsnps[["EA"]][[i]]$MarkerName, 
		file=paste0("Forest/EA_", gsub("_EA*.*", "", names(leadsnps[[1]])[[i]]), "_snps.txt"),
		sep='\t', row.names=F, col.names=F, quote=F)
		}

for(i in 1:length(leadsnps[["AA"]])){
	write.table(leadsnps[["AA"]][[i]]$MarkerName, 
		file=paste0("Forest/AA_", gsub("ARIC/gcta_out_5e-6_extract/|_AA*.*", "", names(leadsnps[[2]])[[i]]), "_snps.txt"),
		sep='\t', row.names=F, col.names=F, quote=F)
		}		

# In Terminal: Extract independent EA SNPs from individual cohort summary stats
cd /Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Forest
phenos=(Gran GrimAge PhenoAge PAI1 Hannum IEAA)
for phen in ${phenos[@]}
do
echo ${phen}
snps=$(cat ./EA_${phen}_snps.txt)
for snp in ${snps}
do
echo ${snp}
grep  ${snp}  ../${phen}/EA/CLEANED.*${phen} >> ./EA_${phen}_leadsnps.txt
done 
done 

# In Terminal: EA (meta-analysed sample)
cd /Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Forest
phenos=(Gran GrimAge PhenoAge PAI1 Hannum IEAA)
for phen in ${phenos[@]}
do
echo ${phen}
snps=$(cat ./EA_${phen}_snps.txt)
for snp in ${snps}
do
echo ${snp}
grep  ${snp}  ../${phen}/${phen}_EA_METAL_HET1.txt >> ./EA_${phen}_meta_leadsnps.txt
done 
done 

# # In Terminal: EA (meta-analysed sample - after excluding studies after het analysis)
# cd /Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Forest
# phenos=(PhenoAge PAI1 Hannum)
# for phen in ${phenos[@]}
# do
# echo ${phen}
# snps=$(cat ./EA_${phen}_snps.txt)
# for snp in ${snps}
# do
# echo ${snp}
# grep  ${snp}  ../PostHet_Metas/${phen}_EA_METAL_HET1.txt >> ./EA_${phen}_PostHetmeta_leadsnps.txt
# done 
# done 


# In Terminal: AA
for phen in ${phenos[@]}
do
echo ${phen}
snps=$(cat ./AA_${phen}_snps.txt)
for snp in ${snps}
do
echo ${snp}
grep  ${snp}  ../${phen}/AA/CLEANED.*${phen} >> ./AA_${phen}_leadsnps.txt
done 
done 

# In Terminal: AA (meta-analysed sample)
cd /Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Forest
phenos=(Gran GrimAge PhenoAge PAI1 Hannum IEAA)
for phen in ${phenos[@]}
do
echo ${phen}
snps=$(cat ./AA_${phen}_snps.txt)
for snp in ${snps}
do
echo ${snp}
grep  ${snp}  ../${phen}/${phen}_AA_METAL_HET1.txt >> ./AA_${phen}_meta_leadsnps.txt
done 
done 


setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Forest")
leadsnps = list()
leadsnps[["EA"]] = list()
leadsnps[["AA"]] = list()
for(i in c("Gran", "GrimAge", "Hannum", "IEAA", "PAI1", "PhenoAge")){
	leadsnps[["EA"]][[i]] = read.table(paste0("EA_", i, "_snps.txt"), header=F)
	leadsnps[["AA"]][[i]] = read.table(paste0("AA_", i, "_snps.txt"), header=F)
}
# EA group
files = dir()[grep("*\\bEA*.*leadsnps.txt", dir())]
meta_files = files[grep("_meta", files)]
files = files[-grep("meta", files)]
fplot = list()
for(file in files){
	fplot[[file]] = read.delim(file, header=F)
	# if(length(grep("GENOA", rownames(fplot[[file]])))==1){
	# genoa = grep("GENOA",rownames(fplot[[file]]))
	# 	for(j in 12:4){
	# 	fplot[[file]][genoa,j] = as.character(fplot[[file]][genoa,j])
	# 	fplot[[file]][genoa,j] = as.character(fplot[[file]][genoa, j-1])
	# 	}
	# 	fplot[[file]][genoa,3] = "+"
	# }
	# Remove underscore from GS,MESA
	rownames(fplot[[file]]) =  gsub(".*CLEANED\\.*", "", fplot[[file]]$V1)
	colnames(fplot[[file]]) = c("cptid", "SNP", "STRAND", "EFFECT_ALLELE",
	"OTHER_ALLELE",	"EAF", "IMPUTATION",
	"BETA",	"SE", "PVAL", "N", "MAC")
	}
x = do.call("rbind", fplot)
# Remove HISP and underscores
# x = x[-grep("HISP", rownames(x)), ]
rownames(x) = gsub("GS_", "GS", rownames(x))
	x$cptid = gsub(".*Gran:|.*PhenoAge:|.*GrimAge:|.*PAI1:|.*IEAA:|.*Hannum:", "", rownames(x))
	x$cohort = gsub("*.*txt\\.*|*_.*", "", rownames(x))
	x$trait = gsub("*.*_|:.*", "", rownames(x))
# Remove any SNPs inadvertently grepped
x = x[-which(!x$cptid %in% as.character(unlist(leadsnps[["EA"]]))),]


meta = list()
for(file in meta_files){
	meta[[file]] = read.delim(file, header=F)
}
y = do.call("rbind", meta)
names(y) = c("MarkerName", 
	"Allele1", "Allele2", 
	"Freq1", "FreqSE", 
	"MinFreq", "MaxFreq", 
	"Effect", "StdErr", 
	"P-value", "Direction", 
	"HetChiSq", "HetDf", "HetPVal", 
	"TotalSampleSize")
	y$trait = gsub("*EA_|*_meta*.*", "", rownames(y))
	y$cohort="Meta"
	y$cptid = as.character(y$MarkerName)
	y$BETA = y$Effect
	y$SE = y$StdErr
    y$EFFECT_ALLELE = toupper(y$Allele1)
    y$OTHER_ALLELE = toupper(y$Allele2)
y = y[which(y$MarkerName %in% x$cptid), ]


# Transform PAI1 (divide beta by 1000)
# This is to permit Mstat calculation
# Exponentiation of large effects creates infinite values 
# and downstream errors in getmstatistic

# Test in IEAA first
test1 = x[x$trait=="IEAA", ]
test1$BETA2 = test1$BETA/1000
test1$Z = test1$BETA/test1$SE
test1$SE2 = test1$BETA2/test1$Z
y2 = y
y2$BETA2 = y2$BETA/1000
y2$Z = y2$BETA/y2$SE
y2$SE2 = y2$BETA2/y2$Z


# Test forest plot
# # Should be identical to IEAA on forestplot.pdf later, but scaled down
# pdf("Forest_Test_IEAATransform2.pdf")
# 	for(snp in unique(test1$cptid)){
# 		plotdata = plotdata1 = test1[test1$trait=="IEAA" & test1$cptid==snp,c("cptid", "cohort", "trait", "BETA2", "SE2", "EFFECT_ALLELE", "OTHER_ALLELE") ]
# 		plotdata = rbind(plotdata, y2[y2$trait=="IEAA" & y2$cptid == snp, c("cptid", "cohort", "trait", "BETA2", "SE2", "EFFECT_ALLELE", "OTHER_ALLELE")])
# 		if(plotdata$EFFECT_ALLELE[nrow(plotdata)] == plotdata$EFFECT_ALLELE[1] & 
# 			plotdata$OTHER_ALLELE[nrow(plotdata)] == plotdata$OTHER_ALLELE[1]){
# 			print("Flipping")
# 			plotdata$BETA2[nrow(plotdata)] = -1*plotdata$BETA2[nrow(plotdata)]
# 			}
# 		plotdata$upper = plotdata$BETA2 + plotdata$SE2
# 		plotdata$lower = plotdata$BETA2 - plotdata$SE2
# 		plotdata$cohort = factor(plotdata$cohort, levels=c("Meta", plotdata1$cohort[rev(order(plotdata1$BETA2))]))
# 		p = ggplot(data=plotdata, aes(x=BETA2, y=cohort)) + 
# 		    geom_point() + 
# 		    geom_vline(aes(xintercept=0)) + 
# 		    ggtitle(paste0(plotdata$trait[1], ": ", snp)) + 
# 		    theme(plot.title = element_text(hjust = 0.5)) +
# 		    geom_errorbarh(data=plotdata, aes(xmin=lower, xmax=upper))
# 		print(p)
# 	}
# dev.off()

# It works - scale down PAI1
x2 = x[x$trait=="PAI1",]
x2$trait = "PAI1/1000"
x2$BETA2 = x2$BETA/1000
x2$Z = x2$BETA/x2$SE
x2$SE2 = x2$BETA2/x2$Z
x2$Z= NULL
x2$BETA = x2$BETA2
x2$SE = x2$SE2
x2$BETA2 = x2$SE2 = NULL
x3 = rbind(x, x2)


# Get m-statistic
require(getmstatistic)
m_results = list()
df_m = list()
for(i in c("Gran", "GrimAge","IEAA", "Hannum", "PhenoAge", "PAI1/1000")){
tmp  = x3[x3$trait==i,c("BETA", "SE", "cohort", "cptid")]
# PAI1 has large effects which don't chuck out an error if exponentiated. 
# Use PAI1/1000 instead and ignore the following loop which simply excludes the larger valuse

# 		if(length(which(is.infinite(exp(tmp$BETA))))>0){
# 		tmp = droplevels(tmp[-which(is.infinite(exp(tmp$BETA))), ])
# 	}
# if(length(which(exp(tmp$BETA)==0))>0){
# 		tmp = droplevels(tmp[-which(exp(tmp$BETA)==0), ])
# 	}

# Study must appear more than once	
		singles = names(table(tmp$cohort))[which(table(tmp$cohort)==1)]
	if(length(singles > 0)){
	tmp = droplevels(tmp[-which(tmp$cohort %in% singles), ])
	}

	m_results[[i]] <- getmstatistic(tmp$BETA,
                                tmp$SE, 
                      			tmp$cptid, 
                    			tmp$cohort, verbose_output=TRUE)
}
names(m_results)[grep("PAI1", names(m_results))] = "PAI1"

influential_studies = do.call("rbind", lapply(m_results, function(x){print(x$influential_studies_0_05)}))
weaker_studies = do.call("rbind", lapply(m_results, function(x){print(x$weaker_studies_0_05)}))

# Extract M-values for cohort-level analyses
mvals = lapply(m_results, function(x){x$M_dataset[,c("study_names_in", "M", "M_se")]})
for(i in names(mvals)){
	mvals[[i]] = cbind(Trait=i, mvals[[i]])
	mvals[[i]] = mvals[[i]][!duplicated(mvals[[i]]),]
}
mvals_df = do.call("rbind", mvals)


# Meta-regression: 
require(stringr)
require(dplyr)
require(metafor)
# Cohort Demographics

demos =  read.csv("cohort_data.csv", stringsAsFactors=F)
demos$Ancestry = gsub(" ", "", demos$Ancestry)
demos$F_prop = str_extract(demos$Female, "\\([^()]+\\)")
demos$F_prop = as.numeric(substr(demos$F_prop, start=2, nchar(demos$F_prop)-1))
demos$age2 = as.numeric(gsub(" \\(.*", "", demos$Age))
demos$age_sd = str_extract_all(demos$Age, "\\([^()]+\\)")
demos$age_sd = substr(demos$age_sd, start=2, nchar(demos$age_sd)-1) %>% as.numeric

demos = demos[demos$Ancestry=="European", ]
demos$Cohort = gsub("ALSPAC ARIES", "ARIES", demos$Cohort)
demos[which(demos$Cohort=="ARIC"), "Cohort"] = "ARICw"
demos$Cohort = gsub("Rotterdam Study ", "RS", demos$Cohort)
demos$Cohort = gsub("Lothian Birth Cohort 19", "LBC", demos$Cohort)
demos$Cohort = gsub("Framingham Heart Study", "FHS", demos$Cohort)
demos$Cohort = gsub("BIB", "BiB", demos$Cohort)
demos$Cohort = gsub("SISTER", "Sister", demos$Cohort)
demos$Cohort = gsub("InCHIANTI", "InChianti", demos$Cohort)



# Panels
panel1 = dir("../../Cohort_Summary_Stats/", recursive=TRUE)[grep("1000G", dir("../../Cohort_Summary_Stats/", recursive=TRUE))]
panel1 = gsub("/.*", "", panel1) %>% unique
panel2 = dir("../../Cohort_Summary_Stats/", recursive=TRUE)[grep("HRC", dir("../../Cohort_Summary_Stats/", recursive=TRUE))]
panel2 = gsub("/.*", "", panel2) %>% unique

# Lambdas 
lamb = read.csv("cohort_lambda.csv")

mvals_df$Cohort_Trait = paste0(mvals_df$study_names_in, "_", mvals_df$Trait)
mvals_df$Cohort_Trait[which(!mvals_df$Cohort_Trait %in% lamb$Cohort_Trait)]
# [1] "FinnTwin_Gran"     "GSw1_Gran"         "GSw3_Gran"
#  [4] "MESA_Gran"         "FinnTwin_GrimAge"  "GSw1_GrimAge"
#  [7] "GSw3_GrimAge"      "MESA_GrimAge"      "FinnTwin_IEAA"
# [10] "GSw1_IEAA"         "GSw3_IEAA"         "MESA_IEAA"
# [13] "FinnTwin_Hannum"   "GSw1_Hannum"       "GSw3_Hannum"
# [16] "MESA_Hannum"       "FinnTwin_PhenoAge" "GSw1_PhenoAge"
# [19] "GSw3_PhenoAge"     "MESA_PhenoAge"     "FinnTwin_PAI1"
# [22] "GSw1_PAI1"         "GSw3_PAI1"         "MESA_PAI1"
mvals_df$Cohort_Trait = gsub("GSw", "GS_w", mvals_df$Cohort_Trait)
mvals_df$Cohort_Trait = gsub("MESA", "MESA_EA", mvals_df$Cohort_Trait)
mvals_df$Cohort_Trait = gsub("FinnTwin", "FTC", mvals_df$Cohort_Trait)

# Skip GS_w1_hannum for now:
mvals_df2 = mvals_df[-which(mvals_df$Cohort_Trait == "GS_w1_Hannum")]
mvals_df2$Lamb = lamb[match(mvals_df2$Cohort_Trait, lamb$Cohort_Trait), "Lambda.PVAL.GC"]
panel1[which(!panel1 %in% mvals_df2$study_names_in)]
# [1] "DTR_GWAS_EpiClocks_Results_20092019" "EasyQC"
# [3] "Epigenetic_Clock_GWAS_ARIC"          "GTP"
# [5] "JHS"                                 "MCCS_GWAS_Epigenetic_Clocks"
# [7] "SAPALDIA_DM"                         "YFS"

panel1 = gsub("DTR_GWAS_EpiClocks_Results_20092019", "DTR", panel1)
panel1 = gsub("Epigenetic_Clock_GWAS_ARIC", "ARICw", panel1)
panel1 = gsub("MCCS_GWAS_Epigenetic_Clocks", "MCCS", panel1)
panel1 = gsub("SAPALDIA_DM", "SAPALDIA", panel1)
panel1[which(!panel1 %in% mvals_df2$study_names_in)]

panel2[which(!panel2 %in% mvals_df2$study_names_in)]
# [1] "EasyQC"
# [2] "Epigenetic_Clock_GWAS_GENOA"
# [3] "FinnTwin_GWAS_clock"
# [4] "Generation_Scotland_Clock"
# [5] "KORA_F4"
# [6] "NetherlandsTwinRegister.EpigeneticClock6Phenotypes.HRC.30April2019.zip"
# [7] "RS2_RS3"
panel2 = gsub("NetherlandsTwinRegister.EpigeneticClock6Phenotypes.HRC.30April2019.zip", "NTR", panel2)
panel2 = gsub("KORA_F4", "KORA", panel2)
panel2 = gsub("Generation_Scotland_Clock", "GSw1", panel2)
panel2 = c(panel2, "GSw3")
panel2 = gsub("RS2_RS3", "RS2", panel2)
panel2 = c(panel2, "RS3")
panel2 = gsub("FinnTwin_GWAS_clock", "FTC", panel2)

mvals_df2$Panel = NA
mvals_df2[mvals_df2$study_names_in %in% panel2, "Panel"] = "HRC"
mvals_df2[mvals_df2$study_names_in %in% panel1, "Panel"] = "1000G"
mvals_df2$Cohort_Trait = gsub("GS_", "GS", mvals_df2$Cohort_Trait)

x3 = x3[-which(x3$trait=="PAI1")]
x3$trait[grep("PAI1", x3$trait)] = "PAI1"
x3$Cohort_Trait = paste0(x3$cohort, "_", x3$trait)
x4 = merge(x3, mvals_df2, by="Cohort_Trait")
x4$Age = demos[match(x4$cohort, demos$Cohort), "age2"]
x4$Age_SD = demos[match(x4$cohort, demos$Cohort), "age_sd"]
x4$Female = demos[match(x4$cohort, demos$Cohort), "F_prop"]

region = list()
region[["Europe"]] = c("AIRWAVE", "BiB", "ARIES", "EGCUT", "InChianti", "GSw1", "GSw3",
	                   "DTR", "KORA", "LBC21", "LBC36", "NTR", "RS2", "RS3", 
	                   "SABRE", "SAPALDIA", "SATSA", "TwinsUK")
region[["USA"]] = c("ARICw", "BHS", "BLSA", "GOLDN", "NIAAA", "Sister")
region[["Australia"]] = c("MCCS")
x4$Region = NA
for(i in names(region)){
x4[which(x4$cohort %in% region[[i]]), "Region"] = i
}

# Number of SNPs per cohort
nsnp = list()
for(trait in unique(x4$Trait)){
	nsnp[[trait]] = list()
for(cohort in unique(x4$cohort)){
	if(cohort == "GSw1"){cohort = "GS_w1"}
	if(cohort == "GSw3"){cohort = "GS_w3"}
	tmpdir = dir(paste0("../", trait, "/EA"))
	tmp = tmpdir[grep(cohort, tmpdir)]
	tmp = system(paste0("wc -l ../", trait, "/EA/", tmp), intern=T)
	nsnp[[trait]][[cohort]] = as.numeric(str_split(tmp, " ",)[[1]][1])
}
nsnp[[trait]] = do.call("rbind", nsnp[[trait]])
}
nsnp2 = do.call("cbind", nsnp)
colnames(nsnp2) = names(nsnp)
rownames(nsnp2) = gsub("_", "", rownames(nsnp2))
rownames(nsnp2)[!rownames(nsnp2)%in%x4$cohort]
x4$NSNP = NA
for(i in colnames(nsnp2)){
	for(j in rownames(nsnp2)){
		x4[x4$cohort==j & x4$trait==i, "NSNP"] = nsnp2[j,i]
	}	
}


model2 = list()
for(trait in unique(x4$Trait)){
model2[[trait]] <- rma(yi = BETA,
              sei = SE,
              data = x4[which(x4$Trait==trait),],
              method = "ML",
              mods = ~ Age + Age_SD + Female + Panel + Lamb + Region + NSNP,
              test="knha")
}



write.table(mvals_df, file="MStats_EA_Cohorts_April2020.txt", sep='\t', quote=F, row.names=F)

# Rename tiffs
for(i in names(m_results)){
	suffix = paste0(m_results[[i]]$number_studies, "studies_", m_results[[i]]$number_variants, "snps.tif")
	files = dir()[grep(suffix, dir())]
	file.rename(files, gsub(".tif", paste0(i, "_EA.tiff"), files))
}

 lapply(m_results, function(x){x$influential_studies_0_05})
# $Gran
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)

# $GrimAge
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)

# $IEAA
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)

# $Hannum
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)

# $PhenoAge
#     Study        M Bonferroni_pvalue
# 27 Sister 1.006995        0.01409521

# $PAI1
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)

# Influential study: Sister (PhenoAge)
# PhenoAge: Sister

posthet_files = dir("../PostHet_Metas", full.names=T)[grep("PhenoAge&HET1.txt$", dir("../PostHet_Metas"))]
meta_posthet = list()
for(file in posthet_files){
	meta_posthet[[file]] = read.delim(file, header=T, stringsAsFactors=F)
}
z = do.call("rbind", meta_posthet)


names(z) = c("MarkerName", 
	"Allele1", "Allele2", 
	"Freq1", "FreqSE", 
	"MinFreq", "MaxFreq", 
	"Effect", "StdErr", 
	"P-value", "Direction", 
#	"HetChiSq", "HetDf", "HetPVal", 
	"TotalSampleSize")
	z$trait = gsub("*EA_|*_PostHetmeta/*.*", "", rownames(z))
	z$cohort="Meta_PostHet"
	z$cptid = as.character(z$MarkerName)
	z$BETA = z$Effect
	z$SE = z$StdErr
    z$EFFECT_ALLELE = toupper(z$Allele1)
    z$OTHER_ALLELE = toupper(z$Allele2)
z3 = z
z3$trait = "PhenoAge"


# Scale down PAI1 in meta-results for forest plot
y2 = y[y$trait=="PAI1",]
y2$BETA2 = y2$BETA/1000
y2$Z = y2$BETA/y2$SE
y2$SE2 = y2$BETA2/y2$Z
y2$trait = "PAI1/1000"
y2$BETA = y2$BETA2
y2$SE = y2$SE2
y2$Z = y2$BETA2 = y2$SE2 =  NULL
y3 = rbind(y, y2)
y3 = y3[,-grep("Het", names(y3))]



require(ggplot2)
pdf("../Plots/ForestPlots.pdf", onefile=T)
for(trait in  c("Gran", "GrimAge","IEAA", "Hannum", "PhenoAge", "PAI1/1000")){
	for(snp in unique(x3[x3$trait==trait, "cptid"])){
		plotdata = plotdata1 = x3[x3$trait==trait & x3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE") ]
		plotdata = rbind(plotdata, y3[y3$trait==trait & y3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE")])
		if(plotdata$EFFECT_ALLELE[nrow(plotdata)] == plotdata$OTHER_ALLELE[1] & 
			plotdata$OTHER_ALLELE[nrow(plotdata)] == plotdata$EFFECT_ALLELE[1]){
			print("Flipping")
			plotdata$BETA[nrow(plotdata)] = -1*plotdata$BETA[nrow(plotdata)]
			}
		plotdata$upper = plotdata$BETA + plotdata$SE
		plotdata$lower = plotdata$BETA - plotdata$SE
		plotdata$cohort = factor(plotdata$cohort, levels=c("Meta", plotdata1$cohort[rev(order(plotdata1$BETA))]))
		p = ggplot(data=plotdata, aes(x=BETA, y=cohort)) + 
		    geom_point() + 
		    geom_vline(aes(xintercept=0)) + 
		    ggtitle(paste0(plotdata$trait[1], ": ", snp)) + 
		    theme(plot.title = element_text(hjust = 0.5)) +
		    geom_errorbarh(data=plotdata, aes(xmin=lower, xmax=upper))
		print(p)
	}
}
dev.off()


# Plot PhenoAge with/without Sister
require(ggplot2)
pdf("../Plots/ForestPlots_PostHet_EUR.pdf", onefile=T)
# for(trait in  c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1/1000")){
for(trait in  "PhenoAge"){
	mysnps = unique(x3[x3$trait==trait & x3$cohort=="Sister", "cptid"])
	for(snp in mysnps){
		plotdata = plotdata1 = x3[x3$trait==trait & x3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE") ]
		plotdata = rbind(plotdata, 
			             y3[y3$trait==trait & y3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE")],
			             z3[z3$trait==trait & z3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE")])
				
				if(plotdata$EFFECT_ALLELE[plotdata$cohort=="Meta_PostHet"] == plotdata$OTHER_ALLELE[1] & 
			   	   plotdata$OTHER_ALLELE[plotdata$cohort=="Meta_PostHet"] == plotdata$EFFECT_ALLELE[1]){
				   		print("Flipping Meta_PostHet")
						plotdata$BETA[plotdata$cohort=="Meta_PostHet"] = -1*plotdata$BETA[plotdata$cohort=="Meta_PostHet"]
						}
		if(plotdata$EFFECT_ALLELE[plotdata$cohort=="Meta"] == plotdata$OTHER_ALLELE[1] & 
		   plotdata$OTHER_ALLELE[plotdata$cohort=="Meta"] == plotdata$EFFECT_ALLELE[1]){
		   		print("Flipping Meta")
				plotdata$BETA[plotdata$cohort=="Meta"] = -1*plotdata$BETA[plotdata$cohort=="Meta"]
				}

		plotdata$upper = plotdata$BETA + plotdata$SE
		plotdata$lower = plotdata$BETA - plotdata$SE
		plotdata$cohort = factor(plotdata$cohort, levels=c("Meta_PostHet", "Meta", plotdata1$cohort[rev(order(plotdata1$BETA))]))
		plotdata$colour=c("black", "red")[as.numeric(levels(plotdata$cohort) %in% c("Sister", "Meta_PostHet"))+1]

		p = ggplot(data=plotdata, aes(x=BETA, y=cohort)) + 
		    geom_point() + 
		    geom_vline(aes(xintercept=0)) + 
		    ggtitle(paste0(plotdata$trait[1], ": ", snp)) + 
		    theme(plot.title = element_text(hjust = 0.5)) +
		    geom_errorbarh(data=plotdata, aes(xmin=lower, xmax=upper)) + 
		    theme(axis.text.y=element_text(colour=plotdata$colour))
		print(p)
	}
}
dev.off()


################################################################################
###############################################################################
y$trait_snp =  paste0(y$trait, "_", y$MarkerName)
z3$trait_snp = paste0(z3$trait, "_", z3$MarkerName)
yy = y[which(y$trait_snp %in% z3$trait_snp), ]
zz = rbind(yy, z3)

require(ggplot2)
		plotdata2 = rbind(y3[,c("EFFECT_ALLELE", "OTHER_ALLELE", "BETA", "cptid", "cohort", "trait", "SE")], 
			             z3[,c("EFFECT_ALLELE", "OTHER_ALLELE", "BETA", "cptid", "cohort", "trait", "SE")])
		hannum_sister =  x[x$cptid %in% plotdata$cptid & x$cohort == "Sister" & x$trait == "Hannum","cptid"]
		pheno_sister =  x[x$cptid %in% plotdata$cptid & x$cohort == "Sister" & x$trait == "PhenoAge","cptid"]
		pai1_lbc21 = x[x$cptid %in% plotdata$cptid & x$cohort == "LBC21" & x$trait == "PAI1","cptid"]
		p1 = plotdata2[plotdata2$cohort %in% c("Meta", "Meta_PostHet") & 
		               plotdata2$trait == "Hannum" & 
		               plotdata2$cptid %in% hannum_sister, ]

		p2 = plotdata2[plotdata2$cohort %in% c("Meta", "Meta_PostHet") & 
		               plotdata2$trait == "PhenoAge" & 
		               plotdata2$cptid %in% pheno_sister, ]	

		p3 = plotdata2[plotdata2$cohort %in% c("Meta", "Meta_PostHet") & 
		               plotdata2$trait == "PAI1" & 
		               plotdata2$cptid %in% pai1_lbc21, ]	

plotdata = p1
plotdata$upper = plotdata$BETA + plotdata$SE
plotdata$lower = plotdata$BETA - plotdata$SE
pdf("Hannum_with_without_sister.pdf")
ggplot(data=p1, aes(y=BETA, x=cptid, shape=cohort)) + 
  			geom_point(position=position_dodge(width=0.5), size = 3) +		    
  			geom_hline(aes(yintercept=0)) + 
		    ggtitle("Hannum with/without Sister Study") + 
		    theme(plot.title = element_text(hjust = 0.5)) +
		    geom_errorbar(data=plotdata, aes(ymin=lower, ymax=upper), 
		    			  position = position_dodge(0.5)) + 
		    coord_flip()
dev.off()


plotdata = p2
plotdata$upper = plotdata$BETA + plotdata$SE
plotdata$lower = plotdata$BETA - plotdata$SE
pdf("PhenoAge_with_without_sister.pdf")
ggplot(data=p2, aes(y=BETA, x=cptid, shape=cohort)) + 
  			geom_point(position=position_dodge(width=0.5), size = 3) +		    
  			geom_hline(aes(yintercept=0)) + 
		    ggtitle("PhenoAge Accel with/without Sister Study") + 
		    theme(plot.title = element_text(hjust = 0.5)) +
		    geom_errorbar(data=plotdata, aes(ymin=lower, ymax=upper), 
		    			  position = position_dodge(0.5)) + 
		    coord_flip()
dev.off()

plotdata = p3
plotdata$upper = plotdata$BETA + plotdata$SE
plotdata$lower = plotdata$BETA - plotdata$SE		
pdf("PAI1_with_without_LBC21.pdf")
ggplot(data=p3, aes(y=BETA, x=cptid, shape=cohort)) + 
  			geom_point(position=position_dodge(width=0.5), size = 3) +		    
  			geom_hline(aes(yintercept=0)) + 
		    ggtitle("PAI1 with/without LBC21") + 
		    theme(plot.title = element_text(hjust = 0.5)) +
		    geom_errorbar(data=plotdata, aes(ymin=lower, ymax=upper), 
		    			  position = position_dodge(0.5)) + 
		    coord_flip()
dev.off()
				if(plotdata$EFFECT_ALLELE[plotdata$cohort=="Meta_PostHet"] == plotdata$OTHER_ALLELE[1] & 
			   	   plotdata$OTHER_ALLELE[plotdata$cohort=="Meta_PostHet"] == plotdata$EFFECT_ALLELE[1]){
				   		print("Flipping Meta_PostHet")
						plotdata$BETA[plotdata$cohort=="Meta_PostHet"] = -1*plotdata$BETA[plotdata$cohort=="Meta_PostHet"]
						}
					} else{
					plotdata = rbind(plotdata, y3[y3$trait==trait & y3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE")])										
					}
		if(plotdata$EFFECT_ALLELE[plotdata$cohort=="Meta"] == plotdata$OTHER_ALLELE[1] & 
		   plotdata$OTHER_ALLELE[plotdata$cohort=="Meta"] == plotdata$EFFECT_ALLELE[1]){
		   		print("Flipping Meta")
				plotdata$BETA[plotdata$cohort=="Meta"] = -1*plotdata$BETA[plotdata$cohort=="Meta"]
				}
		plotdata$upper = plotdata$BETA + plotdata$SE
		plotdata$lower = plotdata$BETA - plotdata$SE
		plotdata$cohort = factor(plotdata$cohort, levels=c("Meta_PostHet", "Meta", plotdata1$cohort[rev(order(plotdata1$BETA))]))
		plotdata$colour = rep("black", nrow(plotdata))
		if(trait == "Hannum" && "Sister" %in% plotdata$cohort){
			plotdata$colour=c("black", "red")[as.numeric(levels(plotdata$cohort) %in% c("Sister", "Meta_PostHet"))+1]
		}
		if(trait == "PhenoAge" && "Sister" %in% plotdata$cohort){
			plotdata$colour=c("black", "red")[as.numeric(levels(plotdata$cohort) %in% c("Sister", "Meta_PostHet"))+1]
		}		
		if(trait == "PAI1/1000" && "LBC21" %in% plotdata$cohort){
			plotdata$colour=c("black", "red")[as.numeric(levels(plotdata$cohort) %in% c("LBC21", "Meta_PostHet"))+1]
		}		
		p = ggplot(data=plotdata, aes(x=BETA, y=cohort)) + 
		    geom_point() + 
		    geom_vline(aes(xintercept=0)) + 
		    ggtitle(paste0(plotdata$trait[1], ": ", snp)) + 
		    theme(plot.title = element_text(hjust = 0.5)) +
		    geom_errorbarh(data=plotdata, aes(xmin=lower, xmax=upper)) + 
		    theme(axis.text.y=element_text(colour=plotdata$colour))
		print(p)
	}
}
dev.off()

		p = ggplot(data=zz, aes(x=BETA, y=cohort)) + 
		    geom_point() + 
		    geom_vline(aes(xintercept=0)) + 
		    ggtitle(paste0(plotdata$trait[1], ": ", snp)) + 
		    theme(plot.title = element_text(hjust = 0.5)) +
		    geom_errorbarh(data=plotdata, aes(xmin=lower, xmax=upper)) + 
		    theme(axis.text.y=element_text(colour=plotdata$colour))
		print(p)




for(i in 12:4){
	x[,i] = as.character(x[,i])
	x[grep("GENOA", rownames(x)), i] = x[grep("GENOA", rownames(x)), i-1]
}
x[grep("GENOA", rownames(x)), 3] = "+"


getmstatistic(beta_in =heartgenes20_10studies$beta_flipped, 
                                   lambda_se_in=heartgenes20_10studies$gcse, 
                            variant_names_in= heartgenes20_10studies$variants, 
                            study_names_in  = heartgenes20_10studies$studies)

getmstatistic(BETA_tmp$BETA, tmp$SE, tmp$cohort, tmp$cptid, 
                    			 verbose_output=TRUE)