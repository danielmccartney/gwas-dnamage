# setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel")


# # Overlap across ancestry groups
# leadsnps = list()
# leadsnps[["EA"]] = list()
# leadsnps[["AA"]] = list()
# files = dir("cojo_files", recursive=T)[grep("_EA/*.*.jma.merge$", dir("cojo_files",recursive=T))]
# for(file in files){
# 	leadsnps[["EA"]][[file]] = read.table(paste0("cojo_files/", file), header=T)
# 	leadsnps[["EA"]][[file]]$MarkerName = paste0(leadsnps[["EA"]][[file]]$Chr, ":", leadsnps[["EA"]][[file]]$bp)
# }
# files = dir("cojo_files", recursive=T)[grep("ARIC/gcta_out_5e-6_extract/*.*5e-8.jma.merge$", dir("cojo_files",recursive=T))]
# for(file in files){
# 	leadsnps[["AA"]][[file]] = read.table(paste0("cojo_files/", file), header=T)
# 	leadsnps[["AA"]][[file]]$MarkerName = paste0(leadsnps[["AA"]][[file]]$Chr, ":", leadsnps[["AA"]][[file]]$bp)
# }

# for(i in 1:length(leadsnps[["EA"]])){
# 	write.table(leadsnps[["EA"]][[i]]$MarkerName, 
# 		file=paste0("Forest/EA_", gsub("_EA*.*", "", names(leadsnps[[1]])[[i]]), "_snps.txt"),
# 		sep='\t', row.names=F, col.names=F, quote=F)
# 		}

# for(i in 1:length(leadsnps[["AA"]])){
# 	write.table(leadsnps[["AA"]][[i]]$MarkerName, 
# 		file=paste0("Forest/AA_", gsub("ARIC/gcta_out_5e-6_extract/|_AA*.*", "", names(leadsnps[[2]])[[i]]), "_snps.txt"),
# 		sep='\t', row.names=F, col.names=F, quote=F)
# 		}		

# # In Terminal: Extract independent EA SNPs from individual cohort summary stats
# cd /Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Forest
# phenos=(Gran GrimAge PhenoAge PAI1 Hannum IEAA)
# for phen in ${phenos[@]}
# do
# echo ${phen}
# snps=$(cat ./EA_${phen}_snps.txt)
# for snp in ${snps}
# do
# echo ${snp}
# grep  ${snp}  ../${phen}/EA/CLEANED.*${phen} >> ./EA_${phen}_leadsnps.txt
# done 
# done 

# # In Terminal: EA (meta-analysed sample)
# cd /Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Forest
# phenos=(Gran GrimAge PhenoAge PAI1 Hannum IEAA)
# for phen in ${phenos[@]}
# do
# echo ${phen}
# snps=$(cat ./EA_${phen}_snps.txt)
# for snp in ${snps}
# do
# echo ${snp}
# grep  ${snp}  ../${phen}/${phen}_EA_METAL_HET1.txt >> ./EA_${phen}_meta_leadsnps.txt
# done 
# done 


# # In Terminal: AA
# for phen in ${phenos[@]}
# do
# echo ${phen}
# snps=$(cat ./AA_${phen}_snps.txt)
# for snp in ${snps}
# do
# echo ${snp}
# grep  ${snp}  ../${phen}/AA/CLEANED.*${phen} >> ./AA_${phen}_leadsnps.txt
# done 
# done 

# # In Terminal: AA (meta-analysed sample)
# cd /Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Forest
# phenos=(Gran GrimAge PhenoAge PAI1 Hannum IEAA)
# for phen in ${phenos[@]}
# do
# echo ${phen}
# snps=$(cat ./AA_${phen}_snps.txt)
# for snp in ${snps}
# do
# echo ${snp}
# grep  ${snp}  ../${phen}/${phen}_AA_METAL_HET1.txt >> ./AA_${phen}_meta_leadsnps.txt
# done 
# done 


setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Forest")
leadsnps = list()
leadsnps[["EA"]] = list()
leadsnps[["AA"]] = list()
for(i in c("Gran", "GrimAge", "Hannum", "IEAA", "PAI1", "PhenoAge")){
	leadsnps[["EA"]][[i]] = read.table(paste0("EA_", i, "_snps.txt"), header=F)
	leadsnps[["AA"]][[i]] = read.table(paste0("AA_", i, "_snps.txt"), header=F)
}
# EA group
files = dir()[grep("*\\bAA*.*leadsnps.txt", dir())]
meta_files = files[grep("_meta", files)]
files = files[-grep("meta", files)]
fplot = list()
for(file in files){
	fplot[[file]] = read.delim(file, header=F, stringsAsFactors=F)
	if(length(grep("GENOA", fplot[[file]]$V1))>=1){
	genoa = grep("GENOA",fplot[[file]]$V1)
		for(j in 12:4){
		fplot[[file]][genoa,j] = as.character(fplot[[file]][genoa, (j-1)])
		}
		fplot[[file]][genoa,3] = "+"
	}
	# Remove underscore from GS,MESA
	rownames(fplot[[file]]) =  gsub(".*CLEANED\\.*", "", fplot[[file]]$V1)
	colnames(fplot[[file]]) = c("cptid", "SNP", "STRAND", "EFFECT_ALLELE",
	"OTHER_ALLELE",	"EAF", "IMPUTATION",
	"BETA",	"SE", "PVAL", "N", "MAC")
	}
x = do.call("rbind", fplot)
# Remove HISP 
x = x[-grep("HISP", rownames(x)), ]
	x$cptid = gsub(".*Gran:|.*PhenoAge:|.*GrimAge:|.*PAI1:|.*IEAA:|.*Hannum:", "", rownames(x))
	x$cohort = gsub("*.*txt\\.*|*_.*", "", rownames(x))
	x$trait = gsub("*.*_|:.*", "", rownames(x))
# Remove any SNPs inadvertently grepped
x = x[-which(!x$cptid %in% as.character(unlist(leadsnps[["AA"]]))),]
for(i in 6:12){
	x[,i] = as.numeric(x[,i])
}

meta = list()
for(file in meta_files){
	meta[[file]] = read.delim(file, header=F)
}
y = do.call("rbind", meta)
names(y) = c("MarkerName", 
	"Allele1", "Allele2", 
	"Freq1", "FreqSE", 
	"MinFreq", "MaxFreq", 
	"Effect", "StdErr", 
	"P-value", "Direction", 
	"HetChiSq", "HetDf", "HetPVal", 
	"TotalSampleSize")
	y$trait = gsub("*AA_|*_meta*.*", "", rownames(y))
	y$cohort="Meta"
	y$cptid = as.character(y$MarkerName)
	y$BETA = y$Effect
	y$SE = y$StdErr
    y$EFFECT_ALLELE = toupper(y$Allele2)
    y$OTHER_ALLELE = toupper(y$Allele1)
y = y[which(y$MarkerName %in% x$cptid), ]


# Transform PAI1 (divide beta by 1000)
# This is to permit Mstat calculation
# Exponentiation of large effects creates infinite values 
# and downstream errors in getmstatistic

# Test in IEAA first
test1 = x[x$trait=="IEAA", ]
test1$BETA2 = test1$BETA/1000
test1$Z = test1$BETA/test1$SE
test1$SE2 = test1$BETA2/test1$Z
y2 = y
y2$BETA2 = y2$BETA/1000
y2$Z = y2$BETA/y2$SE
y2$SE2 = y2$BETA2/y2$Z


# Test forest plot
# # Should be identical to IEAA on forestplot.pdf later, but scaled down
# pdf("Forest_Test_IEAATransform2.pdf")
# 	for(snp in unique(test1$cptid)){
# 		plotdata = plotdata1 = test1[test1$trait=="IEAA" & test1$cptid==snp,c("cptid", "cohort", "trait", "BETA2", "SE2", "EFFECT_ALLELE", "OTHER_ALLELE") ]
# 		plotdata = rbind(plotdata, y2[y2$trait=="IEAA" & y2$cptid == snp, c("cptid", "cohort", "trait", "BETA2", "SE2", "EFFECT_ALLELE", "OTHER_ALLELE")])
# 		if(plotdata$EFFECT_ALLELE[nrow(plotdata)] == plotdata$EFFECT_ALLELE[1] & 
# 			plotdata$OTHER_ALLELE[nrow(plotdata)] == plotdata$OTHER_ALLELE[1]){
# 			print("Flipping")
# 			plotdata$BETA2[nrow(plotdata)] = -1*plotdata$BETA2[nrow(plotdata)]
# 			}
# 		plotdata$upper = plotdata$BETA2 + plotdata$SE2
# 		plotdata$lower = plotdata$BETA2 - plotdata$SE2
# 		plotdata$cohort = factor(plotdata$cohort, levels=c("Meta", plotdata1$cohort[rev(order(plotdata1$BETA2))]))
# 		p = ggplot(data=plotdata, aes(x=BETA2, y=cohort)) + 
# 		    geom_point() + 
# 		    geom_vline(aes(xintercept=0)) + 
# 		    ggtitle(paste0(plotdata$trait[1], ": ", snp)) + 
# 		    theme(plot.title = element_text(hjust = 0.5)) +
# 		    geom_errorbarh(data=plotdata, aes(xmin=lower, xmax=upper))
# 		print(p)
# 	}
# dev.off()

# It works - scale down PAI1
x2 = x[x$trait=="PAI1",]
x2$trait = "PAI1/1000"
x2$BETA2 = x2$BETA/1000
x2$Z = x2$BETA/x2$SE
x2$SE2 = x2$BETA2/x2$Z
x2$Z= NULL
x2$BETA = x2$BETA2
x2$SE = x2$SE2
x2$BETA2 = x2$SE2 = NULL
x3 = rbind(x, x2)


# Get m-statistic
require(getmstatistic)
m_results = list()
df_m = list()
for(i in c("Gran", "GrimAge","IEAA", "Hannum", "PhenoAge", "PAI1/1000")){
tmp  = x3[x3$trait==i,c("BETA", "SE", "cohort", "cptid")]
# PAI1 has large effects which don't chuck out an error if exponentiated. 
# Use PAI1/1000 instead and ignore the following loop which simply excludes the larger valuse

# 		if(length(which(is.infinite(exp(tmp$BETA))))>0){
# 		tmp = droplevels(tmp[-which(is.infinite(exp(tmp$BETA))), ])
# 	}
# if(length(which(exp(tmp$BETA)==0))>0){
# 		tmp = droplevels(tmp[-which(exp(tmp$BETA)==0), ])
# 	}

# Study must appear more than once	
		singles = names(table(tmp$cohort))[which(table(tmp$cohort)==1)]
	if(length(singles > 0)){
	tmp = droplevels(tmp[-which(tmp$cohort %in% singles), ])
	}

	m_results[[i]] <- getmstatistic(tmp$BETA,
                                tmp$SE, 
                      			tmp$cptid, 
                    			tmp$cohort, verbose_output=TRUE)
}
names(m_results)[grep("PAI1", names(m_results))] = "PAI1"

influential_studies = do.call("rbind", lapply(m_results, function(x){print(x$influential_studies_0_05)}))
weaker_studies = do.call("rbind", lapply(m_results, function(x){print(x$weaker_studies_0_05)}))

# Extract M-values for cohort-level analyses
mvals = lapply(m_results, function(x){x$M_dataset[,c("study_names_in", "M", "M_se")]})
for(i in names(mvals)){
	mvals[[i]] = cbind(Trait=i, mvals[[i]])
	mvals[[i]] = mvals[[i]][!duplicated(mvals[[i]]),]
}
mvals_df = do.call("rbind", mvals)

lapply(m_results, function(x){print(x$influential_studies)})
# $Gran
#   Study         M Bonferroni_pvalue
# 2 GENOA 0.7475614       0.000615388
# $GrimAge
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length rowames)
# $IEAA
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)
# $Hannum
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)
# $PhenoAge
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)
# $PAI1
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)


# Meta-regression: 
		demos = demos[demos$Ancestry=="African", ]
demos[which(demos$Cohort=="ARIC"), "Cohort"] = "ARICb"
demos[which(demos$Cohort=="Jackson Heart Study"), "Cohort"] = "JHS"




# Panels
panel1 = dir("../../Cohort_Summary_Stats/", recursive=TRUE)[grep("1000G", dir("../../Cohort_Summary_Stats/", recursive=TRUE))]
panel1 = gsub("/.*", "", panel1) %>% unique
panel2 = dir("../../Cohort_Summary_Stats/", recursive=TRUE)[grep("HRC", dir("../../Cohort_Summary_Stats/", recursive=TRUE))]
panel2 = gsub("/.*", "", panel2) %>% unique

# Lambdas 
lamb = read.csv("cohort_lambda.csv")

mvals_df$Cohort_Trait = paste0(mvals_df$study_names_in, "_", mvals_df$Trait)
mvals_df$Cohort_Trait[which(!mvals_df$Cohort_Trait %in% lamb$Cohort_Trait)]
# [1] "MESA_Gran"     "MESA_GrimAge"  "MESA_Hannum"   "MESA_PhenoAge"
# [5] "MESA_PAI1"

mvals_df$Cohort_Trait = gsub("MESA", "MESA_AA", mvals_df$Cohort_Trait)


# Skip GS_w1_hannum for now:
mvals_df$Lamb = lamb[match(mvals_df$Cohort_Trait, lamb$Cohort_Trait), "Lambda.PVAL.GC"]
panel1[which(!panel1 %in% mvals_df$study_names_in)]
# [1] "BHS"                                 "DTR_GWAS_EpiClocks_Results_20092019"
#  [3] "EasyQC"                              "EGCUT"
#  [5] "Epigenetic_Clock_GWAS_ARIC"          "MCCS_GWAS_Epigenetic_Clocks"
#  [7] "SAPALDIA_DM"                         "SAPALDIA"
#  [9] "SATSA"                               "TwinsUK"
# [11] "YFS"
panel1 = gsub("Epigenetic_Clock_GWAS_ARIC", "ARICb", panel1)

panel2[which(!panel2 %in% mvals_df$study_names_in)]
# [1] "EasyQC"
# [2] "Epigenetic_Clock_GWAS_GENOA"
# [3] "FinnTwin_GWAS_clock"
# [4] "Generation_Scotland_Clock"
# [5] "KORA_F4"
# [6] "NetherlandsTwinRegister.EpigeneticClock6Phenotypes.HRC.30April2019.zip"
# [7] "RS2_RS3"
panel2 = gsub("Epigenetic_Clock_GWAS_GENOA", "GENOA", panel2)
mvals_df$Panel = NA
mvals_df[mvals_df$study_names_in %in% panel2, "Panel"] = "HRC"
mvals_df[mvals_df$study_names_in %in% panel1, "Panel"] = "1000G"

x3 = x3[-which(x3$trait=="PAI1")]
x3$trait[grep("PAI1", x3$trait)] = "PAI1"
x3$Cohort_Trait = paste0(x3$cohort, "_", x3$trait)
x4 = merge(x3, mvals_df, by="Cohort_Trait")
x4$Age = demos[match(x4$cohort, demos$Cohort), "age2"]
x4$Age_SD = demos[match(x4$cohort, demos$Cohort), "age_sd"]
x4$Female = demos[match(x4$cohort, demos$Cohort), "F_prop"]

# region = list()
# region[["Europe"]] = c("AIRWAVE", "BiB", "ARIES", "EGCUT", "InChianti", "GSw1", "GSw3",
# 	                   "DTR", "KORA", "LBC21", "LBC36", "NTR", "RS2", "RS3", 
# 	                   "SABRE", "SAPALDIA", "SATSA", "TwinsUK")
# region[["USA"]] = c("ARICw", "BHS", "BLSA", "GOLDN", "NIAAA", "Sister")
# region[["Australia"]] = c("MCCS")
# x4$Region = NA
# for(i in names(region)){
# x4[which(x4$cohort %in% region[[i]]), "Region"] = i
# }

# Number of SNPs per cohort
nsnp = list()
for(trait in unique(x4$Trait)){
	nsnp[[trait]] = list()
for(cohort in unique(x4$cohort)){
	tmpdir = dir(paste0("../", trait, "/AA"))
	tmp = tmpdir[grep(cohort, tmpdir)]
	tmp = system(paste0("wc -l ../", trait, "/AA/", tmp), intern=T)
	nsnp[[trait]][[cohort]] = as.numeric(str_split(tmp, " ",)[[1]][1])
}
nsnp[[trait]] = do.call("rbind", nsnp[[trait]])
}
nsnp2 = do.call("cbind", nsnp)
colnames(nsnp2) = names(nsnp)
rownames(nsnp2) = gsub("_", "", rownames(nsnp2))
rownames(nsnp2)[!rownames(nsnp2)%in%x4$cohort]
x4$NSNP = NA
for(i in colnames(nsnp2)){
	for(j in rownames(nsnp2)){
		x4[x4$cohort==j & x4$trait==i, "NSNP"] = nsnp2[j,i]
	}	
}


model2 = list()
for(trait in unique(x4$Trait)){
model2[[trait]] <- rma(yi = BETA,
              sei = SE,
              data = x4[which(x4$Trait==trait),],
              method = "ML",
              mods = ~ Age + Age_SD + Female + Lamb + NSNP + Panel,
              test="knha")
				}

unique(x4$cohort)


write.table(mvals_df, file="MStats_AA_Cohorts_April2020.txt", sep='\t', quote=F, row.names=F)

# Rename tiffs
for(i in names(m_results)){
	suffix = paste0(m_results[[i]]$number_studies, "studies_", m_results[[i]]$number_variants, "snps.tif")
	files = dir()[grep(suffix, dir())]
	file.rename(files, gsub(".tif", paste0(i, "_AA.tiff"), files))
}

 lapply(m_results, function(x){x$influential_studies_0_05})
# $Gran
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)

# $GrimAge
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)

# $IEAA
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)

# $Hannum
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)

# $PhenoAge
#     Study        M Bonferroni_pvalue
# 27 Sister 1.006995        0.01409521

# $PAI1
# [1] Study             M                 Bonferroni_pvalue
# <0 rows> (or 0-length row.names)

# Influential study: Sister (PhenoAge)
# Gran: GENOA

posthet_files = "../PostHet_Metas/Gran_AA_METAL_HET1.txt"
meta_posthet = list()
for(file in posthet_files){
	meta_posthet[[file]] = read.delim(file, header=T, stringsAsFactors=F)
}
z = do.call("rbind", meta_posthet)


names(z) = c("MarkerName", 
	"Allele1", "Allele2", 
	"Freq1", "FreqSE", 
	"MinFreq", "MaxFreq", 
	"Effect", "StdErr", 
	"P-value", "Direction", 
#	"HetChiSq", "HetDf", "HetPVal", 
	"TotalSampleSize")
	z$trait = gsub("*EA_|*_PostHetmeta/*.*", "", rownames(z))
	z$cohort="Meta_PostHet"
	z$cptid = as.character(z$MarkerName)
	z$BETA = z$Effect
	z$SE = z$StdErr
    z$EFFECT_ALLELE = toupper(z$Allele2)
    z$OTHER_ALLELE = toupper(z$Allele1)
z3 = z
z3$trait = "Gran"


# Scale down PAI1 in meta-results for forest plot
y2 = y[y$trait=="PAI1",]
y2$BETA2 = y2$BETA/1000
y2$Z = y2$BETA/y2$SE
y2$SE2 = y2$BETA2/y2$Z
y2$trait = "PAI1/1000"
y2$BETA = y2$BETA2
y2$SE = y2$SE2
y2$Z = y2$BETA2 = y2$SE2 =  NULL
y3 = rbind(y, y2)
y3 = y3[,-grep("Het", names(y3))]



# Plot Gran SNPs with/without GENOA
require(ggplot2)
pdf("../Plots/BoxPlots_PostHet_AFR.pdf", onefile=T)
# for(trait in  c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1/1000")){
for(trait in  "Gran"){
	mysnps = unique(x3[x3$trait==trait & x3$cohort=="GENOA", "cptid"])
	for(snp in mysnps){
		plotdata = plotdata1 = x3[x3$trait==trait & x3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE") ]
		plotdata = rbind(plotdata, 
			             y3[y3$trait==trait & y3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE")],
			             z3[z3$trait==trait & z3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE")])
				
				if(plotdata$EFFECT_ALLELE[plotdata$cohort=="Meta_PostHet"] == plotdata$EFFECT_ALLELE[1] & 
			   	   plotdata$OTHER_ALLELE[plotdata$cohort=="Meta_PostHet"] == plotdata$OTHER_ALLELE[1]){
				   		print("Flipping Meta_PostHet")
						plotdata$BETA[plotdata$cohort=="Meta_PostHet"] = -1*plotdata$BETA[plotdata$cohort=="Meta_PostHet"]
						}
		if(plotdata$EFFECT_ALLELE[plotdata$cohort=="Meta"] == plotdata$EFFECT_ALLELE[1] & 
		   plotdata$OTHER_ALLELE[plotdata$cohort=="Meta"] == plotdata$OTHER_ALLELE[1]){
		   		print("Flipping Meta")
				plotdata$BETA[plotdata$cohort=="Meta"] = -1*plotdata$BETA[plotdata$cohort=="Meta"]
				}

		plotdata$upper = plotdata$BETA + plotdata$SE
		plotdata$lower = plotdata$BETA - plotdata$SE
		plotdata$cohort = factor(plotdata$cohort, levels=c("Meta_PostHet", "Meta", plotdata1$cohort[rev(order(plotdata1$BETA))]))
		plotdata$colour=c("black", "red")[as.numeric(levels(plotdata$cohort) %in% c("GENOA", "Meta_PostHet"))+1]

		p = ggplot(data=plotdata, aes(y=BETA, x=cohort)) + 
		    geom_boxplot() + 
		    geom_hline(aes(yintercept=0)) + 
		    ggtitle(paste0(plotdata$trait[1], ": ", snp)) + 
		    theme(plot.title = element_text(hjust = 0.5)) +
		    geom_errorbar(data=plotdata, aes(xmin=lower, xmax=upper)) + 
		    theme(axis.text.y=element_text(colour=plotdata$colour))
		print(p)
	}
}
dev.off()



#multiplot
# Plot Gran SNPs with/without GENOA
require(ggplot2)
p = list()
plotlist = list()
pdf("../Plots/ForestPlots_PostHet_AFR.pdf", onefile=T)
# for(trait in  c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1/1000")){
for(trait in  "Gran"){
	mysnps = unique(x3[x3$trait==trait & x3$cohort=="GENOA", "cptid"])
	for(snp in mysnps){
		plotdata = plotdata1 = x3[x3$trait==trait & x3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE") ]
		plotdata = rbind(plotdata, 
			             y3[y3$trait==trait & y3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE")],
			             z3[z3$trait==trait & z3$cptid == snp, c("cptid", "cohort", "trait", "BETA", "SE", "EFFECT_ALLELE", "OTHER_ALLELE")])
				
				if(plotdata$EFFECT_ALLELE[plotdata$cohort=="Meta_PostHet"] == plotdata$EFFECT_ALLELE[1] & 
			   	   plotdata$OTHER_ALLELE[plotdata$cohort=="Meta_PostHet"] == plotdata$OTHER_ALLELE[1]){
				   		print("Flipping Meta_PostHet")
						plotdata$BETA[plotdata$cohort=="Meta_PostHet"] = -1*plotdata$BETA[plotdata$cohort=="Meta_PostHet"]
						}
		if(plotdata$EFFECT_ALLELE[plotdata$cohort=="Meta"] == plotdata$EFFECT_ALLELE[1] & 
		   plotdata$OTHER_ALLELE[plotdata$cohort=="Meta"] == plotdata$OTHER_ALLELE[1]){
		   		print("Flipping Meta")
				plotdata$BETA[plotdata$cohort=="Meta"] = -1*plotdata$BETA[plotdata$cohort=="Meta"]
				}

		plotdata$upper = plotdata$BETA + plotdata$SE
		plotdata$lower = plotdata$BETA - plotdata$SE
		plotdata$cohort = factor(plotdata$cohort, levels=c("Meta_PostHet", "Meta", plotdata1$cohort[rev(order(plotdata1$BETA))]))
		plotdata$colour=c("black", "red")[as.numeric(levels(plotdata$cohort) %in% c("GENOA", "Meta_PostHet"))+1]
plotlist[[snp]] = plotdata
}
}

plotlist2=do.call("rbind", plotlist)
plotlist2$title = paste0(plotlist2$trait[1], ": ", plotlist2$cptid)
		ggplot(data=plotlist2, aes(x=BETA, y=cohort)) + 
		    geom_point() + 
		    geom_vline(aes(xintercept=0)) + 
		    facet_wrap(.~title, ncol=3) + 
		    theme(plot.title = element_text(hjust = 0.5)) +
		    geom_errorbarh(data=plotdata, aes(xmin=lower, xmax=upper)) + 
		    theme(axis.text.y=element_text(colour=plotdata$colour))	
		}
}
dev.off()


