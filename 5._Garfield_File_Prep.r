# Garfield
setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel")
# Manhattan plot meta analysis
source("/Cluster_Filespace/Marioni_Group/Daniel/Age_EWAS/manhattan.R")

clocks = c("Hannum", "IEAA", "GrimAge", 
           "PhenoAge", "PAI1", "Gran")




x = list()
for(clock in clocks){
x[[clock]] = read.table(paste0(clock, "/", clock, "_EA_METAL_HET1.txt"), sep='\t', stringsAsFactors=F, header=T)
x[[clock]] = x[[clock]][which(x[[clock]]$TotalSampleSize >= (max(x[[clock]]$TotalSampleSize)/2)), ]
}

setwd("Garfield/garfield-data/pval/")
for(clock in clocks){
	dir.create(paste0(clock, "_EA"))
}

require(stringr)
for(clock in clocks){
x[[clock]]$chr = gsub(":.*", "", x[[clock]]$MarkerName)
x[[clock]]$pos = unlist(lapply(str_split(x[[clock]]$MarkerName,":"), function(x){x[2]}))
}

for(clock in clocks){
for(chr in unique(x[[clock]]$chr)){
	tmp = x[[clock]][which(x[[clock]]$chr==chr), ]
	tmp = tmp[,c("pos", "P.value")]
	tmp = tmp[order(tmp$pos), ]
	filename = paste0(clock, "_EA/chr", chr)
	print(paste0(clock, " chr ", chr))
	write.table(tmp, file=filename, sep=" ", col.names=F, row.names=F, quote=F)
}
}
