# Gran GWAS lookup
require(data.table)
require(gdata)

setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel")
sig_eur = read.table("Tables/EUR_Hits.txt", header=T, stringsAsFactors=F)
sig_eur = sig_eur[sig_eur$Trait=="Gran",]
sig_eur$MarkerName = paste0(sig_eur$Chr, ":", sig_eur$bp)
# gran  = fread("gran_N169822_narrow_form.tsv")
gran = read.delim("/home/dmccartn/igv_gwsig/gran.gwas", header=T, stringsAsFactors=F)

require(stringr)
marker = str_split(gran$VARIANT, "_")
gran$Marker = unlist(lapply(marker, function(x){x[1]}))


astle = read.xls("Astle_TableS3.xlsx", header=T, stringsAsFactors=F, skip=1)
astle = astle[grep("GRAN#", astle[,3]),]
astle$MarkerName = paste0(astle$Chr..GRCh37.,":",astle$BP..GRCh37.)

gran2 = data.frame(gran)[which(gran$Marker %in% astle$MarkerName),]
# Multiple entries for 15:42250306 (indel) - keep only the most frequent one
gran2 = gran2[-which(gran2$VARIANT == "15:42250306_A_AGATTGTTG")]

meta_results_eur = fread("Gran/Gran_EA_METAL_HET1.txt", header=T, stringsAsFactors=F)
meta_results_afr = fread("Gran/Gran_AA_METAL_HET1.txt", header=T, stringsAsFactors=F)


int1 = intersect(as.data.frame(gran)[, "Marker"], sig_eur$MarkerName)
	x1 = meta_results_eur[which(meta_results_eur$MarkerName %in% int1), ]
	x1$Allele1 = toupper(x1$Allele1)
	x1$Allele2 = toupper(x1$Allele2)
	y1 = data.frame(gran)[which(gran$Marker %in% int1), ]
	y1$colour = ifelse(y1[,"P"] < 5e-8, "red", "black")
	# Remove those not matching A1/A2 codes
	y1 = y1[match(x1$MarkerName, y1$Marker), ]
	y1$PlotBeta = y1$EFFECT
	y1[which(y1$ALT == x1$Allele2),"PlotBeta"] = -1*y1[which(y1$ALT == x1$Allele2),"PlotBeta"]

int2 = intersect(gran2$Marker, as.data.frame(meta_results_eur)$MarkerName)
	x2 = data.frame(meta_results_eur)[which(meta_results_eur$MarkerName %in% int2), ]
	x2$Allele1 = toupper(x2$Allele1)
	x2$Allele2 = toupper(x2$Allele2)
	y2 = as.data.frame(gran2)[which(gran2$Marker %in% int2), ]
	y2 = y2[match(x2$MarkerName, y2$Marker), ]
	y2$PlotBeta = y2$EFFECT
    remove =  which(y2$ALT != x2$Allele1 & y2$ALT != x2$Allele2)
    x2 = x2[-remove,]
    y2 = y2[-remove,]
	y2[which(y2$ALT == x2$Allele2),"PlotBeta"] = -1*y2[which(y2$ALT == x2$Allele2),"PlotBeta"]
	x2$colour = ifelse(x2[,"P.value"] < 5e-8, "red", "black")

pdf("Plots/Gran_GWAS_Effects.pdf")
par(mfrow=c(1,2))
plot(x1$Effect, y1$PlotBeta, col=y1$colour,  xlab="FACS Granulocyte GWAS", ylab="DNAm Granulocyte GWAS", main="DNAm GWAS \nSignificant SNPs")
abline(0,1, h=0, v=0)
plot(x2$Effect, y2$PlotBeta, col=x2$colour,  xlab="DNAm Granulocyte GWAS", ylab="FACS Granulocyte GWAS", main="FACS GWAS \nSignificant SNPs")
abline(0,1, h=0, v=0)
dev.off()



astle[match(sig_eur$MarkerName, astle$MarkerName), ]
supp_table = x2[,c("MarkerName","P.value", "Allele1", "Allele2", "Effect", "StdErr", "TotalSampleSize")]
supp_table$chr = gsub(":.*", "", supp_table$MarkerName)
supp_table$pos = gsub(".*:", "", supp_table$MarkerName)
supp_table$rsid = y2[match(x2$MarkerName, y2$Marker), "ID"]
supp_table$clock= "Granulocyte proportions"

supp_table = supp_table[,c("rsid","chr","pos","P.value", 
	                       "Allele1", "Allele2", "Effect", 
	                       "StdErr", "TotalSampleSize", "clock")]
names(supp_table) = c("SNP","Chromosome", "Position", 
					  "P.value", "A1", "A2", "Effect", 
					  "StdErr", "TotalSampleSize", "Clock")	                      
supp_table = supp_table[order(supp_table$P.value),]
write.table(supp_table, file="Tables/Astle_LeadSNPs_EUR_Meta.txt", sep="\t", quote=F, row.names=F)