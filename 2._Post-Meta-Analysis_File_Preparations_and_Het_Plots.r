setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel")
# Load dependencies and required functions
source("/Cluster_Filespace/Marioni_Group/Daniel/Age_EWAS/manhattan.R")
source("qqunif.plot.R")
require(data.table)
require(dplyr)


clocks = c("Hannum", "IEAA", "GrimAge", 
           "PhenoAge", "PAI1", "Gran")

# Define lambda function 
getlambda = function(x){
pvalue <- x[,grep("P.Value|P.value", colnames(x))]
chisq <- qchisq(1-pvalue,1)
# For z-scores as association, just square them
    # chisq <- data$z^2
        #For chi-squared values, keep as is
        #chisq <- data$chisq
    lambda = median(chisq)/qchisq(0.5,1)
    return(signif(lambda,3))
}

################################################################
# Heterogeneity Plots (I2)
################################################################
# Plot het by p-val
# dir.create("Het_plots")
x = list()
for(clock in clocks){
x[[clock]] = read.table(paste0(clock, "/", clock, "_EA_METAL_HET1.txt"), sep='\t', stringsAsFactors=F, header=T)
x[[clock]] = x[[clock]][which(x[[clock]]$TotalSampleSize >= (max(x[[clock]]$TotalSampleSize)/2)), ] # Only consider studies with >=0.5 meta sample size


# Calculate I2
x[[clock]]$I2 = ((x[[clock]]$HetChiSq-x[[clock]]$HetDf)/x[[clock]]$HetChiSq) * 100
x[[clock]][which(x[[clock]]$I2 < 0), "I2"] = 0
tmp <- x[[clock]][which(x[[clock]]$TotalSampleSize >= (max(x[[clock]]$TotalSampleSize)/2)), ] # Only consider studies with >=0.5 meta sample size

# tmp = tmp[tmp$P.value<1e-4, ]
tmp$MarkerName = gsub(":ID", "", tmp$MarkerName)
tmp$SNP <- tmp$MarkerName
tmp$CHR <- gsub( ":.*$", "", tmp$MarkerName)
if(isTRUE(grep("rs", tmp$MarkerName))){
tmp = tmp[-grep("rs", tmp$MarkerName), ]
}
tmp$CHR <- gsub("X:", "23:", tmp$CHR)
tmp = tmp[-grep("23", tmp$MarkerName), ]
tmp$CHR <- as.numeric(as.character(tmp$CHR))
tmp$POS <- gsub( ".*:", "", tmp$MarkerName)
tmp$POS <- as.numeric(as.character(tmp$POS))
tmp$P <- tmp$P.value
ymax = max(-log10(tmp$P)) + 5

# Plot manhattan plots while we're here
print(png(paste0("manhattan_", clock, "_EA_meta_23042020.png"), width = 1200, height = 800))
print(manhattan(tmp, snp="SNP", chr="CHR", bp="POS", p="P", col=c("firebrick1","firebrick4"), ylim=c(0,ymax),
	suggestiveline=F, genomewideline=-log10(5e-8), main=paste0(clock, "_EUR")))
print(dev.off())
}

# Back up x in case it's overwritten below
x2 = x

# I2 density plots for Additional File
for(clock in clocks){
m = ggplot(x[[clock]], aes(x = I2, y = abs(Effect))) +
xlab("I2")+
ylab("Absolute Effect") +
stat_density_2d(aes(fill = ..level..), geom="polygon")+
png(paste0("Plots/", clock, "_EUR_I2_vs_Effect.png"))
print(m)
print(dev.off())
}


# Plot I2 vs effect (deciles)
for(i in clocks){
deciles = x2[[i]]$dec = ntile(x2[[i]]$I2, 10)
dec_min = min(sort(deciles[which(x2[[i]]$I2!=0)]))
x2[[i]]$plotdec = x2[[i]]$dec
lt = which(x2[[i]]$plotdec < dec_min)
gt = which(x2[[i]]$plotdec >= dec_min)
x2[[i]]$plotdec[lt] = paste0("Decile 1-", dec_min-1)
x2[[i]]$plotdec[gt] = paste0("Decile ", x2[[i]]$plotdec[gt])
# Get plotting order right
factorlevels = order(as.numeric(gsub("Decile 1-|Decile ", "", sort(levels(as.factor(x2[[i]]$plotdec))))))[-1]
x2[[i]]$plotdec = factor(gsub("Decile ", "", x2[[i]]$plotdec), levels = c(paste0("1-", dec_min-1),
													 gsub("Decile ", "", levels(as.factor(x2[[i]]$plotdec)))[factorlevels]))
x2[[i]]$xlab
for(j in levels(x2[[i]]$plotdec)){
x2[[i]][x2[[i]]$plotdec==j,"xlab"] = paste0(j, "\n(", 
	                                        signif(min(x2[[i]][x2[[i]]$plotdec==j,"I2"]),2),"-", 
	                                        signif(max(x2[[i]][x2[[i]]$plotdec==j,"I2"]),2), ")")
}
}

tiff("Het_plots/I2_vs_Effect_deciles_13052020.tiff", width = 8, height = 8, units = 'in', res = 150)
	par(mfrow=c(2,3))
for(clock in clocks){
	legend = paste0("I2 Decile\n", levels(x2[[clock]]$plotdec))
	title= ifelse(clock == "Gran", "Granulocytes", clock)
	print(boxplot(abs(x2[[clock]]$Effect)~as.factor(x2[[clock]]$plotdec),
	           xlab="I2 Decile", ylab="Abs. Effect", main=title))
	}
	dev.off()

tiff("Het_plots/I2_vs_Effect_deciles_13052020_allsnps.tiff", width = 8, height = 8, units = 'in', res = 150)
	par(mfrow=c(2,3))
for(clock in clocks){
	title= ifelse(clock == "Gran", "Granulocytes", clock)
	print(boxplot(abs(x2[[clock]]$Effect)~as.factor(x2[[clock]]$xlab),
	           xlab="I2 Decile", ylab="Abs. Effect", main=title))
	}
	dev.off()


	tiff("Het_plots/I2_vs_pval_24042020.tiff", width = 8, height = 8, units = 'in', res = 150)
	par(mfrow=c(2,3))
for(clock in clocks){
	title= ifelse(clock == "Gran", "Granulocytes", clock)
	print(plot(-log10(x[[clock]][which(x[[clock]]$P.value<0.001), "P.value"]),
				x[[clock]][which(x[[clock]]$P.value<0.001), "I2"],
	           xlab="-log10(pGWAS)", ylab="I2", main=title), xlim=c(0,100))
	print(abline(v=-log10(5e-8), col="red"))
	}
	dev.off()
	tiff("Het_plots/het_p_vs_p_24042020.tiff", width = 8, height = 8, units = 'in', res = 150)
	par(mfrow=c(2,3))
for(clock in clocks){
	title= ifelse(clock == "Gran", "Granulocytes", clock)
		print(plot(-log10(x[[clock]][which(x[[clock]]$P.value<0.001), "P.value"]),
	           -log10(x[[clock]][which(x[[clock]]$P.value<0.001), "HetPVal"]),
	           xlab="-log10(pGWAS)", ylab="-log10(pHet)", main=title))
	print(abline(v=-log10(5e-8), col="red"))
	}
	dev.off()
}



############### Repeat above for AFR analysis
y = y2 = list()
for(clock in clocks){
y[[clock]] = fread(paste0(clock, "/", clock, "_AA_METAL_HET1.txt"), sep='\t', header=T, stringsAsFactors=FALSE)
y2[[clock]] = y[[clock]]
y[[clock]] <- y[[clock]][which(y[[clock]]$TotalSampleSize >= (max(y[[clock]]$TotalSampleSize)/2)), ]

# Heterogeneity measures (Calculate I2)
y[[clock]]$I2 = ((y[[clock]]$HetChiSq-y[[clock]]$HetDf)/y[[clock]]$HetChiSq) * 100
y[[clock]][which(y[[clock]]$I2 < 0), "I2"] = 0
}
y2= lapply(y, as.data.frame)

	# I2 density plots
		for(clock in clocks){
		m = ggplot(y[[clock]][y[[clock]]$"P-value" < 0.001,], aes(x = I2, y = abs(Effect))) +
		# geom_point() +
		xlab("I2")+
		ylab("Absolute Effect") +
		# stat_density_2d(aes(fill = ..level..), geom="polygon") +
	    geom_density_2d(bins=100) +
		xlim(c(0,100)) 
		png(paste0("Plots/", clock, "_AFR_I2_vs_Effect.png"))
		print(m)
		print(dev.off())
		}

		for(clock in clocks){
		m = ggplot(x[[clock]][x[[clock]]$"P-value" < 0.001,], aes(x = I2, y = abs(Effect))) +
		# geom_point() +
		xlab("I2")+
		ylab("Absolute Effect") +
		# stat_density_2d(aes(fill = ..level..), geom="polygon") +
	    geom_density_2d(bins=100) +
		xlim(c(0,100)) 
		png(paste0("Plots/", clock, "_EUR_I2_vs_Effect.png"))
		print(m)
		print(dev.off())
		}

###############################################################    
# Examine inflation with and without MHC
###############################################################

for(clock in clocks){
y2[[clock]]$chr = gsub(":.*", "", y2[[clock]]$MarkerName)
y2[[clock]]$pos = gsub(".*:", "", y2[[clock]]$MarkerName)
}

# MHC = chr6:28,477,797-33,448,354
nomhc = y2
for(clock in clocks){
	nomhc[[clock]] = nomhc[[clock]][-which(nomhc[[clock]]$chr==6 & nomhc[[clock]]$pos %in% 28477797:33448354),]
}
lambdas_mhc = lapply(y, getlambda)
lambdas_nomhc = lapply(nomhc, getlambda)

for(clock in clocks){
	y[[clock]]$P2 = y[[clock]]$P.value
	y[[clock]]$P2[which(y[[clock]]$P2<1e-10)] = 1e-10
	x[[clock]]$P2 = x[[clock]]$P.value
	x[[clock]]$P2[which(x[[clock]]$P2<1e-10)] = 1e-10
}

# qqplots with mhc
for(clock in clocks){
	lambda = getlambda(y[[clock]])
print(pdf(paste0("Plots/qq_AFR_", clock, ".pdf")))
p = qqunif.plot(y[[clock]]$P2, main=paste0("AFR_",clock, "\n lambda = ", lambda))
print(plot(p))
print(dev.off())
}
for(clock in clocks){
		lambda = getlambda(x[[clock]])

print(pdf(paste0("Plots/qq_EUR_", clock, ".pdf")))
p = qqunif.plot(x[[clock]]$P2, main=paste0("EUR_",clock, "\n lambda = ", lambda))
print(plot(p))
print(dev.off())
}




for(clock in clocks){
	tiff("Het_plots/I2_vs_Effect_AFR_03032020.tiff", width = 8, height = 8, units = 'in', res = 150)
	par(mfrow=c(2,3))
for(clock in clocks){
	title= ifelse(clock == "Gran", "Granulocytes", clock)
	print(plot((y[[clock]][which(y[[clock]]$P.value<0.001), "I2"]),
	           abs(y[[clock]][which(y[[clock]]$P.value<0.001), "Effect"]),
	           xlab="I2", ylab="Abs. Effect", main=title), xlim=c(0,100))
	}
	dev.off()
	tiff("Het_plots/I2_vs_pval_AFR_24042020.tiff", width = 8, height = 8, units = 'in', res = 150)
	par(mfrow=c(2,3))
for(clock in clocks){
	title= ifelse(clock == "Gran", "Granulocytes", clock)
	print(plot(-log10(y[[clock]][which(y[[clock]]$P.value<0.001), "P.value"]),
				y[[clock]][which(y[[clock]]$P.value<0.001), "I2"],
	           xlab="-log10(pGWAS)", ylab="I2", main=title), xlim=c(0,100))
	print(abline(v=-log10(5e-8), col="red"))
	}
	dev.off()
	tiff("Het_plots/het_p_vs_p_AFR_24042020.tiff", width = 8, height = 8, units = 'in', res = 150)
	par(mfrow=c(2,3))
for(clock in clocks){
	title= ifelse(clock == "Gran", "Granulocytes", clock)
		print(plot(-log10(y[[clock]][which(y[[clock]]$P.value<0.001), "P.value"]),
	           -log10(y[[clock]][which(y[[clock]]$P.value<0.001), "HetPVal"]),
	           xlab="-log10(pGWAS)", ylab="-log10(pHet)", main=title))
	print(abline(v=-log10(5e-8), col="red"))
	}
	dev.off()
}

##############################
## AFR Meta Manhattan Plots ##
##############################
for(clock in clocks){
tmp <- y[[clock]][which(y[[clock]]$TotalSampleSize >= (max(y[[clock]]$TotalSampleSize)/2)), ]
tmp$MarkerName = gsub(":ID", "", tmp$MarkerName)
tmp$SNP <- tmp$MarkerName
tmp$CHR <- gsub( ":.*$", "", tmp$MarkerName)
if(isTRUE(grep("rs", tmp$MarkerName))){
tmp = tmp[-grep("rs", tmp$MarkerName), ]
}
tmp$CHR <- gsub("X", "23", tmp$CHR)
tmp = tmp[-grep("23", tmp$MarkerName), ]
tmp$CHR <- as.numeric(as.character(tmp$CHR))
tmp$POS <- gsub( ".*:", "", tmp$MarkerName)
tmp$POS <- as.numeric(as.character(tmp$POS))
tmp$P <- tmp$P.value
ymax = max(-log10(tmp$P)) + 5
print(png(paste0("manhattan_", clock, "_AA_meta_23042020.png"), width = 1200, height = 800))
print(manhattan(tmp, snp="SNP", chr="CHR", bp="POS", p="P", col=c("firebrick1","firebrick4"), ylim=c(0,ymax),
	suggestiveline=F, genomewideline=-log10(5e-8), main=paste0(clock, "_AFR")))
print(dev.off())
}

########################################
# Prep meta data for LDHub/MRBase/FUMA #
########################################
setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel")
# Read reference data
map_ea1 = read.table("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/HRC_1000G_maps/HRC.r1-1.GRCh37.wgs.mac5.sites.tab.rsid_map", sep='\t', header=T, stringsAsFactors=FALSE)
map_ea2 = read.table("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/HRC_1000G_maps/1000GP_p3v5_legends_rbind.noDup.noMono.noCnv.noCnAll.afref.EUR.txt", sep='\t', header=T, stringsAsFactors=FALSE)
ldhub = read.table("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/w_hm3.noMHC.snplist", header=T, stringsAsFactors=FALSE)

ea = x
ea_ld = list()
for(clock in clocks){

ea[[clock]] = ea[[clock]][which(ea[[clock]]$TotalSampleSize >= (max(ea[[clock]]$TotalSampleSize)/2)), ]	
ea[[clock]]$rsid_1000G = as.character(map_ea2[match(ea[[clock]]$MarkerName, map_ea2$cptid), "cptid"])
map_ea1$rsid2 = paste0(map_ea1$chr, ":", map_ea1$pos)
ea[[clock]]$hrc_rsid = as.character(map_ea1[match(ea[[clock]]$MarkerName, map_ea1$rsid2), "rsid"])
ea[[clock]]$MarkerName = as.character(ea[[clock]]$MarkerName)
ea[[clock]][match(map_ea1[[clock]]$rsid2, ea[[clock]]$MarkerName), "MarkerName"]
ea[[clock]]$Chr = gsub(":.*", "", ea[[clock]]$MarkerName)
ea[[clock]]$Pos = gsub(".*:", "", x[[clock]]$MarkerName)

names(ea[[clock]])[c(2,3)] = c("A1","A2")
ea[[clock]][,2] = toupper(ea[[clock]][,2])
ea[[clock]][,3] = toupper(ea[[clock]][,3])
ea[[clock]]$zscore = ea[[clock]]$Effect/ea[[clock]]$StdErr
ea[[clock]]$SNP = ea[[clock]]$hrc_rsid
ea[[clock]][is.na(ea[[clock]]$SNP), "SNP"] = ea[[clock]]$rsid_1000G[is.na(ea[[clock]]$SNP)]
ea_ld[[clock]] = ea[[clock]][which(ea[[clock]]$hrc_rsid %in% ldhub$SNP), ]


 ld_upload = ea_ld[[clock]][ea_ld[[clock]]$SNP %in% ldhub$SNP, c("SNP","A1","A2","zscore","TotalSampleSize", "P.value") ]
 names(ld_upload) = c("snpid", "A1", "A2", "Zscore", "N", "P-value")
 write.table(ld_upload, file=paste0(clock, "_EA_for_LDHub_04032020.txt"), sep='\t', quote=F, row.names=F)
 #system(paste0("zip ", clock, "_EA_for_LDHub.txt", clock, "EA_for_LDHub.zip"))

 # Prepare for MRBase
mrbase = ea[[clock]][, c("SNP", "Effect","StdErr", "A1","A2", "Freq1", "P.value", "TotalSampleSize")]
names(mrbase) = c("SNP", "beta", "se", "effect_allele", "other_allele", "eaf", "P","N")
write.table(mrbase, file=paste0(clock, "_MRBase_04032020.txt"), sep='\t', quote=F, row.names=F)

# 30 March 2020: JM requested chr/pos in Becky's file
josine = ea[[clock]][, c("SNP", "Chr", "Pos", "Effect","StdErr", "A1","A2", "Freq1", "P.value", "TotalSampleSize")]
names(josine) = c("SNP", "chr", "pos", "beta", "se", "effect_allele", "other_allele", "eaf", "P","N")
write.table(josine, file=paste0(clock, "_MRBase_ChrPos_30032020.txt"), sep='\t', quote=F, row.names=F)


# Prepare for Fuma
 fuma = ea[[clock]][,c("SNP", "P.value", "A1", "A2", "Effect", "StdErr", "TotalSampleSize")]
 write.table(fuma, file=paste0(clock, "_EA_for_Fuma_04032020.txt"), sep='\t', quote=F, row.names=F)
}

aa = y
map_aa1 = read.table("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/HRC_1000G_maps/HRC.r1-1.GRCh37.wgs.mac5.sites.tab.rsid_map", sep='\t', header=T, stringsAsFactors=F)
map_aa2 = read.table("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/HRC_1000G_maps/1000GP_p3v5_legends_rbind.noDup.noMono.noCnv.noCnAll.afref.AFR.txt", sep='\t', header=T, stringsAsFactors=F)

for(clock in clocks){
aa[[clock]] = aa[[clock]][which(aa[[clock]]$TotalSampleSize >= (max(aa[[clock]]$TotalSampleSize)/2)), ]	
aa[[clock]]$rsid_1000G = as.character(map_aa2[match(aa[[clock]]$MarkerName, map_aa2$cptid), "cptid"])
map_aa1$rsid2 = paste0(map_aa1$chr, ":", map_aa1$pos)
aa[[clock]]$hrc_rsid = as.character(map_aa1[match(aa[[clock]]$MarkerName, map_aa1$rsid2), "rsid"])
aa[[clock]]$MarkerName = as.character(aa[[clock]]$MarkerName)
aa[[clock]][match(map_aa1[[clock]]$rsid2, aa[[clock]]$MarkerName), "MarkerName"]

names(aa[[clock]])[c(2,3)] = c("A1","A2")
aa[[clock]][,2] = toupper(aa[[clock]][,2])
aa[[clock]][,3] = toupper(aa[[clock]][,3])
aa[[clock]]$zscore = aa[[clock]]$Effect/aa[[clock]]$StdErr


aa[[clock]]$SNP = aa[[clock]]$hrc_rsid
aa[[clock]][is.na(aa[[clock]]$SNP), "SNP"] = aa[[clock]]$rsid_1000G[is.na(aa[[clock]]$SNP)]


# Prepare for Fuma
 fuma = aa[[clock]][,c("SNP", "P.value", "A1", "A2", "Effect", "StdErr", "TotalSampleSize")]
 write.table(fuma, file=paste0(clock, "_nonEA_for_Fuma_04032020.txt"), sep='\t', quote=F, row.names=F)
}

###########################
# Prepare files for COJO ##
###########################
setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/")
require(stringr)
clocks = c("Hannum", "IEAA", "GrimAge", 
           "PhenoAge", "PAI1", "Gran")
           
x = list()
# map_ea1 = read.table("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/HRC_1000G_maps/HRC.r1-1.GRCh37.wgs.mac5.sites.tab.rsid_map", sep='\t', header=T, stringsAsFactors=FALSE)
# map_ea2 = read.table("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/HRC_1000G_maps/1000GP_p3v5_legends_rbind.noDup.noMono.noCnv.noCnAll.afref.EUR.txt", sep='\t', header=T, stringsAsFactors=FALSE)

for(clock in clocks){
x[[clock]] = read.table(paste0(clock, "/", clock, "_EA_METAL_HET1.txt"), sep='\t', stringsAsFactors=F, header=T)
x[[clock]] = x[[clock]][which(x[[clock]]$TotalSampleSize >= (max(x[[clock]]$TotalSampleSize)/2)), ]
}


for(clock in clocks){
# x[[clock]]$rsid_1000G = as.character(map_ea2[match(x[[clock]]$MarkerName, map_ea2$cptid), "cptid"])
# map_ea1$rsid2 = paste0(map_ea1$chr, ":", map_ea1$pos)
x[[clock]]$Allele1 = toupper(x[[clock]]$Allele1)
x[[clock]]$Allele2 = toupper(x[[clock]]$Allele2)
# Ensure all A1s are minor alleles
x[[clock]]$A1_new = x[[clock]]$Allele1
x[[clock]]$A2_new = x[[clock]]$Allele2
x[[clock]]$freq_new = x[[clock]]$Freq1
x[[clock]]$effect_new = x[[clock]]$Effect
ind = which(x[[clock]]$Freq1>0.5)
x[[clock]]$freq_new[ind] = 1-x[[clock]]$Freq1[ind]
x[[clock]]$A1_new[ind] = x[[clock]]$Allele2[ind]
x[[clock]]$A2_new[ind] = x[[clock]]$Allele1[ind]
x[[clock]]$effect_new[ind] = (-x[[clock]]$Effect[ind])
x[[clock]]$chr = gsub(":.*", "", x[[clock]]$MarkerName)
x[[clock]]$pos = gsub(".*:", "", x[[clock]]$MarkerName)
x[[clock]]$MarkerName2 = paste0(x[[clock]]$chr, "_", x[[clock]]$pos, "_", x[[clock]]$A2_new, "_", x[[clock]]$A1_new)
}

cojo_in = lapply(x, function(x){x[,c("MarkerName2", "A1_new", "A2_new", "freq_new", "effect_new", "StdErr", "P.value", "TotalSampleSize")]})

for(clock in clocks){
	names(cojo_in[[clock]]) = c("SNP", "A1", "A2", "freq", "b", "se", "p", "N")
	write.table(cojo_in[[clock]], file=paste0("COJO_EA_", clock, "_minoralleles.txt"), sep='\t', quote=F, row.names=F)
}

sigs = list()
for(i in names(cojo_in)){
	sigs[[i]] = cojo_in[[i]]$SNP[cojo_in[[i]]$p<5e-8]
	sigs[[i]] = unique(gsub("_.*", "", sigs[[i]]))
	write.table(data.frame(sigs[[i]]), file=paste0(i, "_cojo_sig_chrs"), 
		        quote=F, row.names=F, col.names=F)
	tmp = cojo_in[[i]]$SNP[which(cojo_in[[i]]$p<5e-8)]
	write.table(data.frame(tmp), file=paste0(i, "_cojo_sig_snps"), 
		        quote=F, row.names=F, col.names=F)
}

# GS: prep unrelated plink files
unrel1 = read.table("/Cluster_Filespace/Marioni_Group/GS/GS_methylation/GS_meth_grm_unrelated_05.grm.id")
unrel2 = read.csv("/Cluster_Filespace/Marioni_Group/GS/GS_methylation/wave3-final/samplesheet.final.csv", header=T)
famids = read.table("/GWAS_Source/GS_GWAS/GS20K/GS20k_TopStrand.fam")
unrel2_2 = data.frame(V1=famids[match(unrel2$Sample_Name, famids$V2), "V1"],
                      V2= unrel2$Sample_Name)
unrelateds = rbind(unrel1, unrel2_2)
write.table(data.frame(unrelateds[,2], unrelateds[,2]), quote=F, col.names=F, row.names=F, file="/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/cojo_files/unrelateds.id")