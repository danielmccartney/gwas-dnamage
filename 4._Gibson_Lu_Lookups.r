# Cojo hits EUR vs AFR
setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/cojo_files/")
require(data.table)

horvath_meta = fread("../IEAA_EA_for_Fuma_04032020.txt", stringsAsFactors=F, header=T)
hannum_meta = fread("../Hannum_EA_for_Fuma_04032020.txt", stringsAsFactors=F, header=T)

lu_ieaa = c("rs11706810", "rs2736099", "rs143093668", "rs6915893", "rs73397619", "rs78781855")
lu_eeaa = c("rs10937913", "rs71007656", "rs1005277")

gibson_horvath=c("rs1011267",
			  "rs79070372",
			  "rs388649",
			  "rs6440667",
			  "rs2736099",
			  "rs7744541",
			  "rs76244256",
			  "rs4712953",
			  "rs10778517",
			  "rs62078811")
gibson_hannum = "rs1005277"

hannum_for_rep = unique(c(lu_eeaa, gibson_hannum))
horvath_for_rep = unique(c(lu_ieaa, gibson_horvath))


eur = afr = list()

for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){
	eur[[i]] = read.table(paste0(i, "_EA/", i, ".jma.merge"), header=T, stringsAsFactors=F)
	afr[[i]] = read.table(paste0("ARIC/results_5e-6_extract/", i, "_AA.jma.merge"), header=T)
	
	eur[[i]]$SNP = gsub("_._.", "", eur[[i]]$SNP)
	eur[[i]]$SNP = gsub("_", ":", eur[[i]]$SNP)
	afr[[i]]$SNP = gsub("_._.", "", afr[[i]]$SNP)
	afr[[i]]$SNP = gsub("_", ":", afr[[i]]$SNP)
}


meta_results_eur = meta_results_afr = list()
for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){
	meta_results_eur[[i]] = fread(paste0("../", i, "/", i, "_EA_METAL_HET1.txt"), header=T, stringsAsFactors=F)
	meta_results_afr[[i]] = fread(paste0("../", i, "/", i, "_AA_METAL_HET1.txt"), header=T, stringsAsFactors=F)
}



afr_in_eur = eur_in_afr = list()
for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){
	afr_in_eur[[i]] = meta_results_eur[[i]][which(meta_results_eur[[i]]$MarkerName %in% afr[[i]]$SNP), ]
	eur_in_afr[[i]] = meta_results_afr[[i]][which(meta_results_afr[[i]]$MarkerName %in% eur[[i]]$SNP), ]
}

clocks = c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")
sig_eur = sig_afr = sig_int = list()


# Fuma input SNP overlap	
fuma_eur = fuma_afr = list()
for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){
	fuma_eur[[i]] = fread(paste0("../", i, "_EA_for_Fuma_04032020.txt"), header=T, stringsAsFactors=F)
	fuma_afr[[i]] = fread(paste0("../", i, "_nonEA_for_Fuma_04032020.txt"), header=T, stringsAsFactors=F)
}
clocks = c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")
sig_eur = sig_afr = sig_int = list()
for(clock in clocks){
	sig_eur[[clock]] = length(which(fuma_eur[[clock]]$P.value < 5e-8))
	sig_afr[[clock]] = length(which(fuma_afr[[clock]]$P.value < 5e-8))
	sig_int[[clock]] = length(intersect(as.data.frame(fuma_eur[[clock]])[which(fuma_eur[[clock]]$P.value < 5e-8),"SNP"], 
		 								as.data.frame(fuma_afr[[clock]])[which(fuma_afr[[clock]]$P.value < 5e-8),"SNP"]))
	}


require(venn)
trans_anc = list()
for(clock in clocks){
	trans_anc[[clock]] = list()
	trans_anc[[clock]][["eur"]] = as.data.frame(fuma_eur[[clock]])[which(fuma_eur[[clock]]$P.value < 5e-8),"SNP"]
	trans_anc[[clock]][["afr"]] = as.data.frame(fuma_afr[[clock]])[which(fuma_afr[[clock]]$P.value < 5e-8),"SNP"]
}

eur_venn = list()
for(clock in clocks){
	eur_venn[[clock]] = as.data.frame(fuma_eur[[clock]])[which(fuma_eur[[clock]]$P.value < 5e-8),"SNP"]
}



# Table of hits and lookups
map_aa3 = fread("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Cohort_Summary_Stats/EasyQC/Allele_Freq_and_Mapping_Info/rsmid_machsvs_mapb37.1000G_p3v5.merged_mach_impute.v3.corrpos.gz", sep='\t', header=T, stringsAsFactors=FALSE)
map_aa3$markername = paste0(map_aa3$chr, ":", map_aa3$pos)
cojo_eur = cojo_afr = sig_eur = sig_afr = list()

clocks = c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")
for(clock in clocks){
	cojo_eur[[clock]] = read.table(paste0(clock, "_EA/", clock, ".jma.merge"), header=T, stringsAsFactors=F)
	cojo_eur[[clock]]$MarkerName = paste0(cojo_eur[[clock]]$Chr, ":", cojo_eur[[clock]]$bp)
	cojo_eur[[clock]]$rsID = data.frame(map_aa3)[match(cojo_eur[[clock]]$MarkerName, map_aa3$markername), "rsmid"]

	cojo_afr[[clock]] = read.table(paste0("ARIC/gcta_out_5e-6_extract/", clock, "_AA_5e-8.jma.merge"), header=T, stringsAsFactors=F)
	cojo_afr[[clock]]$MarkerName = paste0(cojo_afr[[clock]]$Chr, ":", cojo_afr[[clock]]$bp)
    cojo_afr[[clock]]$rsID = data.frame(map_aa3)[match(cojo_afr[[clock]]$MarkerName, map_aa3$markername), "rsmid"]

	sig_eur[[clock]] = cojo_eur[[clock]][,c("SNP", "rsID", "Chr", "bp", "b", "se", "p")]
	sig_eur[[clock]]$b_afr = data.frame(meta_results_afr[[clock]])[match(cojo_eur[[clock]]$MarkerName, meta_results_afr[[clock]]$MarkerName), "Effect"]
	sig_eur[[clock]]$se_afr = data.frame(meta_results_afr[[clock]])[match(cojo_eur[[clock]]$MarkerName, meta_results_afr[[clock]]$MarkerName), "StdErr"]
	sig_eur[[clock]]$p_afr = data.frame(meta_results_afr[[clock]])[match(cojo_eur[[clock]]$MarkerName, meta_results_afr[[clock]]$MarkerName), "P.value"]
	sig_eur[[clock]]$Trait = clock

	sig_afr[[clock]] = cojo_afr[[clock]][,c("SNP", "rsID", "Chr", "bp", "b", "se", "p")]
	sig_afr[[clock]]$b_eur = data.frame(meta_results_eur[[clock]])[match(cojo_afr[[clock]]$MarkerName, meta_results_eur[[clock]]$MarkerName), "Effect"]
	sig_afr[[clock]]$se_eur = data.frame(meta_results_eur[[clock]])[match(cojo_afr[[clock]]$MarkerName, meta_results_eur[[clock]]$MarkerName), "StdErr"]
	sig_afr[[clock]]$p_eur = data.frame(meta_results_eur[[clock]])[match(cojo_afr[[clock]]$MarkerName, meta_results_eur[[clock]]$MarkerName), "P.value"]
	sig_afr[[clock]]$Trait = clock
	}

# write.table(do.call("rbind", sig_eur), file="../Tables/EUR_Hits.txt", quote=F, row.names=F, sep='\t')
# write.table(do.call("rbind", sig_afr), file="../Tables/AFR_Hits.txt", quote=F, row.names=F, sep='\t')
