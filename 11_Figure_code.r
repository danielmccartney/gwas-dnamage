# Miami plots for paper

setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel")
# Manhattan plot meta analysis
source("/Cluster_Filespace/Marioni_Group/Daniel/Age_EWAS/manhattan.R")
source("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Plots/miami.r")

clocks = c("Hannum", "IEAA", "GrimAge", 
           "PhenoAge", "PAI1", "Gran")
x = list()
for(clock in clocks){
x[[clock]] = read.table(paste0(clock, "_EA_METAL1.txt"), sep='\t', header=T)
x[[clock]] = x[[clock]][which(x[[clock]]$TotalSampleSize >= (max(x[[clock]]$TotalSampleSize)/2)), ]
x[[clock]] = x[[clock]][which(x[[clock]]$P.value < 1e-3), ]
# x[[clock = x[[clock[x[[clock]]$P.value<1e-4, ]
x[[clock]]$MarkerName = gsub(":ID", "", x[[clock]]$MarkerName)
x[[clock]]$SNP <- x[[clock]]$MarkerName
x[[clock]]$CHR <- gsub( ":.*$", "", x[[clock]]$MarkerName)
if(isTRUE(grep("rs", x[[clock]]$MarkerName))){
x[[clock]] = x[[clock]][-grep("rs", x[[clock]]$MarkerName), ]
}

x[[clock]]$CHR <- gsub("X
	:", "23:", x[[clock]]$CHR)
x[[clock]] = x[[clock]][-grep("23", x[[clock]]$MarkerName), ]
x[[clock]]$CHR <- as.numeric(as.character(x[[clock]]$CHR))
x[[clock]]$POS <- gsub( ".*:", "", x[[clock]]$MarkerName)
x[[clock]]$POS <- as.numeric(as.character(x[[clock]]$POS))
x[[clock]]$P <- x[[clock]]$P.value
}



y = list()
for(clock in clocks){
y[[clock]] = read.table(paste0(clock, "_nonEA_METAL1.txt"), sep='\t', header=T)
y[[clock]] = y[[clock]][which(y[[clock]]$TotalSampleSize >= (max(y[[clock]]$TotalSampleSize)/2)), ]
y[[clock]] = y[[clock]][which(y[[clock]]$P.value < 1e-3), ]
# y[[clock = y[[clock[y[[clock]]$P.value<1e-4, ]
y[[clock]]$MarkerName = gsub(":ID", "", y[[clock]]$MarkerName)
y[[clock]]$SNP <- y[[clock]]$MarkerName
y[[clock]]$CHR <- gsub( ":.*$", "", y[[clock]]$MarkerName)
if(isTRUE(grep("rs", y[[clock]]$MarkerName))){
y[[clock]] = y[[clock]][-grep("rs", y[[clock]]$MarkerName), ]
}

y[[clock]]$CHR <- gsub("X:", "23:", y[[clock]]$CHR)
y[[clock]] = y[[clock]][-grep("23", y[[clock]]$MarkerName), ]
y[[clock]]$CHR <- as.numeric(as.character(y[[clock]]$CHR))
y[[clock]]$POS <- gsub( ".*:", "", y[[clock]]$MarkerName)
y[[clock]]$POS <- as.numeric(as.character(y[[clock]]$POS))
y[[clock]]$P <- y[[clock]]$P.value
}
saveRDS(x, file="Plots/EA_clocks_miami.rds")
saveRDS(y, file="Plots/nonEA_clocks_miami.rds")


# Change working directory path for dataset, source, and tiff output

# Make plots
x = readRDS("Plots/EA_clocks_miami.rds")
y = readRDS("Plots/nonEA_clocks_miami.rds")

names(x) = names(y) = c("Hannum AgeAccel",
	         "IEAA", "Grim AgeAccel", "Pheno AgeAccel",
	         "PAI1", "Graunlocytes")

x_miami = lapply(x, function(x){cbind(x, Data = "EA")})
y_miami = lapply(y, function(x){cbind(x, Data = "nonEA")})

for(i in names(x)){
	miamiplot = rbind(x_miami[[i]], y_miami[[i]])
tiff(paste0("Plots/", i, "_miami.tiff"), res=300, width=3200, height=1800)
miami(miamiplot, genomewideline=c(-log10(5e-8),log10(5e-8)), suggestiveline=c(-log10(1e-5),log10(1e-5)), bp="POS",data.lower="nonEA", 
      ylim=c(-20,20), yaxt="n", 
      main=paste0(i))
axis(side = 2, at=seq(-20,20,2), labels=c(seq(20,0,-2), seq(2,20,2)))

print(dev.off())
}




# Polygenic plots
source("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Polygenic/pgrs_r2.r")
require(ggplot2)
require(reshape2)
require(RColorBrewer)
require(dplyr)

 plot_36 = melt(out36, id="thresh")
 plot_36$Cohort = "LBC36"
 plot_21 = melt(out21, id="thresh")
 plot_21$Cohort = "LBC21"

unique(plot_21$variable)
# [1] ieaa_r2   hannum_r2 pheno_r2  grim_r2   gran_r2   pai_r2

# YFS Data from Pashu (email 02/04/2020)
grim_r2  <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("grim_r2",6), value= c(0.01,0.41,0.32,0.12,0.21,0.17), Cohort=rep("YFS", 6))
gran_r2 <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("gran_r2",6), value= c(0.61,0.19,0.35,0.46,0.59,0.56), Cohort=rep("YFS", 6))
ieaa_r2 <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("ieaa_r2",6), value= c(3.37,1.17,0.71,0.43,0.23,0.22), Cohort=rep("YFS", 6))
pai1_r2 <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("pai1_r2",6), value= c(0.67,0.31,0.22,0.30,0.11,0.09), Cohort=rep("YFS", 6))
pheno_r2 <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("pheno_r2",6), value= c(0.34,0.44,0.56,0.50,0.12,0.11), Cohort=rep("YFS", 6))
hannum_r2 <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("hannum_r2",6), value= c(0.61,0.18,0.01,0.02,0.01,0.01), Cohort=rep("YFS", 6))

plot_yfs = rbind(grim_r2, gran_r2, ieaa_r2, pai1_r2, pheno_r2, hannum_r2)

fhs = read.csv("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Polygenic/FHS_PGS.csv")
names(fhs) = c("X", "P<5e8", "P<0.01", "P<0.05", "P<0.1", "P<0.5", "P<1")
fhs = melt(fhs)
fhs$Cohort = "FHS"

fhs$X = gsub("GrimAge", "grim_r2", fhs$X)
fhs$X = gsub("Gran", "gran_r2", fhs$X)
fhs$X = gsub("IEAA", "ieaa_r2", fhs$X)
fhs$X = gsub("Hannum", "hannum_r2", fhs$X)
fhs$X = gsub("PAI1", "pai1_r2", fhs$X)
fhs$X = gsub("PhenoAge", "pheno_r2", fhs$X)

names(fhs) = c("variable", "thresh", "value", "Cohort")
fhs = fhs[,c("thresh", "variable", "value", "Cohort")]

bristol = read.table("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Polygenic/results_prs_aries_bib_sabre.txt", header=T)
bristol = melt(bristol)
names(bristol)[2] = "thresh"
bristol = bristol[,c("thresh", "variable", "value", "Cohort")]
bristol$Cohort = gsub("Born_in_Bradford", "BiB", bristol$Cohort)

 plotdata = rbind(plot_36, plot_21, plot_yfs, fhs, bristol)
 plotdata$Cohort = factor(plotdata$Cohort, levels=c("LBC21", "LBC36", "YFS", "FHS", "ARIES", "BiB", "SABRE"))
 plotdata$value = as.numeric(plotdata$value)
 plotdata$variable = gsub("_r2", "", plotdata$variable)
 plotdata$variable = gsub("gran|Gran", "Granulocytes", plotdata$variable)
 plotdata$variable = gsub("grim|GrimAge", "Grim AgeAccel", plotdata$variable)
 plotdata$variable = gsub("pheno|PhenoAge", "Pheno AgeAccel", plotdata$variable)
 plotdata$variable = gsub("ieaa", "IEAA", plotdata$variable)
 plotdata$variable = gsub("hannum|Hannum", "Hannum AgeAccel", plotdata$variable)
 plotdata$variable = gsub("pai|pai1", "PAI1", plotdata$variable)
 colours = brewer.pal(9, "Set1")[1:7]
 company_colors <-c("#E50000", "#008A8A", "#AF0076", "#E56800", "#1717A0", "#E5AC00", "#00B700")
 plotdata$thresh = gsub("P<5e8", "P<5e-8", plotdata$thresh)
 plotdata$thresh[is.na(plotdata$value)] = "P<5e-8"
 plotdata$thresh = factor(plotdata$thresh, levels = c("P<5e-8", "P<0.01", "P<0.05", "P<0.1", "P<0.5", "P<1"))
 tiff("Plots/PGRS_Percent_variance_explained_April2020.tiff",res=300, width=1800, height=1800)
 ggplot(data=plotdata, aes(fill=Cohort, y=value, x=thresh)) +
  geom_boxplot(aes(group=thresh), outlier.shape=NA, show_guide=FALSE) +
  # geom_point(position=position_jitterdodge(), size=2, aes(colour=Cohort)) +
 facet_wrap(.~variable) + 
 scale_y_continuous(expand = c(0, 0), limits = c(0, 5)) +
 theme(axis.text.x = element_text(angle = 45, vjust=0.5)) +
 xlab("P-Value Threshold") + 
 ylab("% Variance Explained")  + 
 scale_colour_manual(values=company_colors)
 dev.off()

 for(i in unique(plotdata$variable)){
 	print(mean(plotdata[plotdata$variable==i, "value"], na.rm=T))
 }

  for(i in unique(plotdata$variable)){
 	print(summary(plotdata[plotdata$variable==i, "value"], na.rm=T))
 }



 # h2
 setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/LDHub/March2020")
require(stringr)
h2_extract = system('grep "Observed scale h2" *h2.log', intern=T)
h2 = data.frame(Predictor = gsub("_EA.*", "", h2_extract),
	            h2 = as.numeric(gsub(" \\(.*|.*: ", "", h2_extract)),
	            se =  as.numeric(gsub("\\)|.*\\(", "", h2_extract)))

h2$lower_se = h2$h2-h2$se
h2$upper_se = h2$h2+h2$se

h2$Predictor = gsub("Gran", "Granulocytes", h2$Predictor)
h2$Predictor = gsub("Hannum", "Hannum AgeAccel", h2$Predictor)
h2$Predictor = gsub("PhenoAge", "Pheno AgeAccel", h2$Predictor)
h2$Predictor = gsub("GrimAge", "Grim AgeAccel", h2$Predictor)
write.table(h2, file="../../Tables/h2_080320.csv", sep=',',row.names=F, quote=F)

require(ggplot2)
tiff("../../Plots/heritability_080320.tiff",res=300, width=1800, height=1800)
ggplot(data=h2, aes(x=Predictor, y=h2)) +
ylim(0,0.2) +
geom_point() + 
geom_errorbar(ymin=h2$lower_se, ymax=h2$upper_se, width=0.25) + 
labs(y = expression (~h^2)) +
ggtitle("Heritability Analysis") + theme(plot.title = element_text(hjust = 0.5)) + 
theme(axis.text.x = element_text(angle = 90, vjust=0.25)) 
dev.off()


# RG Heatmap
require(ComplexHeatmap)
setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/LDHub/March2020")
files = dir(recursive=T)[grep(".csv", dir(recursive=T))]
csv = list()
for(file in files){
	csv[[file]] = read.csv(file)
}
csvs = do.call("rbind", csv)
rownames(csvs) = 1:nrow(csvs)

csvs = csvs[csvs$ethnicity=="European", ]
csvs = csvs[order(csvs$p), ]
csvs = csvs[-grep("Caution", csvs$note), ]
csvs = csvs[-which(is.na(csvs$rg)), ]
csvs = droplevels(csvs)
csvs$trait1 = factor(csvs$trait1, levels=sort(unique(csvs$trait1)))
csvs = csvs[order(csvs$trait1), ]
table(csvs$trait1)
csvs$trait1 = gsub("rg.", "", csvs$trait1)
csvs$trait1 = gsub("_EA_08032020|EA_00|_EA_08042020", "", csvs$trait1)
csvs$trait1 = gsub("Gran", "Granulocytes", csvs$trait1)
csvs$trait1 = gsub("GrimAge", "Grim AgeAccel", csvs$trait1)
csvs$trait1 = gsub("PhenoAge", "Pheno AgeAccel", csvs$trait1)
csvs$trait1 = gsub("Hannum", "Hannum AgeAccel", csvs$trait1)
write.table(csvs, file="../../Tables/genetic_correlations_14042020.csv", sep=',', row.names=F, quote=F)

traits = length(unique(csvs$trait2))  # Unique traits
# [1] 693    
thresh = 0.05/(traits)
x = csvs[csvs$p<thresh,]

tmp = reshape(csvs[,c("trait1", "trait2", "rg")], idvar = "trait2", timevar = "trait1", direction = "wide")
rows = tmp[,1]
tmp[,1] = NULL
colnames(tmp) = gsub("rg.", "", colnames(tmp))
tmp = apply(tmp, 2, as.numeric)
rownames(tmp) = rows
tmp[is.na(tmp)] = 0


cor_traits = c("Age at Menarche",
				"Age at Menopause",
"Age of first birth",
"Alzheimers disease",
"Anorexia Nervosa",
"Asthma",
"Attention deficit hyperactivity disorder",
"Autism spectrum disorder",
"Bipolar disorder",
"Birth weight",
"Body mass index",
"Celiac disease",
"Childhood IQ",
"College completion",
"Crohns disease",
"Depressive symptoms",
"Ever vs never smoked",
"Fathers age at death",
"FEV1/FVC",
"Forced expiratory volume in 1 second",
"Forced vital capacity",
"Glucose",
"HbA1C",
"HDL cholesterol",
"Height_2010",
"ICV",
"Inflammatory Bowel Disease (Euro)",
"Intelligence",
"LDL cholesterol",
"Lung cancer",
"Major depressive disorder",
"Mean Accumbens",
"Mean Caudate",
"Mean Hippocampus",
"Mean Pallidum",
"Mean platelet volume",
"Mean Putamen",
"Mean Thalamus",
"Mothers age at death",
"Multiple sclerosis",
"Neuroticism",
"Pack Years",
"Parents age at death",
"Parkinsons disease",
"Rheumatoid Arthritis",
"Serum total cholesterol",
"Serum total triglycerides",
"Total Cholesterol",
"Triglycerides",
"Type 2 Diabetes",
"Ulcerative colitis",
"Waist-to-hip ratio",
"Years of schooling 2016")


pdf("../../Plots/rg_heatmap_allelefix.pdf")
Heatmap(tmp[which(rownames(tmp) %in% cor_traits),], 
	     row_names_gp = gpar(fontsize = 8),
	     heatmap_legend_param = list(title = "rg"),
	     cluster_rows=FALSE, cluster_columns=FALSE)
dev.off()

csvs1 = csvs[which(csvs$p < 2.23e-4),]
csvs2 = csvs[which(csvs$p<0.05),]

dat = read.delim("../../Tables/rg_table_6traits.xls", header=T)
dat = dat[-grep("PAI1", dat$p1),]
dat = dat[-grep("PAI1", dat$p2),]

plotdat = matrix(nrow=5, ncol=5)
rownames(plotdat) = colnames(plotdat) = unique(c(as.character(dat$p1), as.character(dat$p2)))

for(i in rownames(plotdat)){
for(j in rownames(plotdat)){
	if(i==j){plotdat[i,j] = 1} else{
	plotdat[i,j] = dat[which(dat$p1 %in% c(i,j) & dat$p2 %in% c(i,j)), "rg"]
		}
	}
	}
colnames(plotdat) = rownames(plotdat) = gsub("Gran", "Granulocytes", rownames(plotdat))
colnames(plotdat) = rownames(plotdat) = gsub("Age", "AgeAccel", rownames(plotdat))
plotdat[upper.tri(plotdat)] = NA
plotdat[plotdat==1] = NA

plotdat2 = matrix(nrow=5, ncol=5)
rownames(plotdat2) = colnames(plotdat2) = unique(c(as.character(dat$p1), as.character(dat$p2)))

for(i in rownames(plotdat2)){
for(j in rownames(plotdat2)){
	if(i==j){plotdat2[i,j] = 1} else{
	plotdat2[i,j] = paste0(round(dat[which(dat$p1 %in% c(i,j) & dat$p2 %in% c(i,j)), "rg"],2), "\n",
		"(", round(dat[which(dat$p1 %in% c(i,j) & dat$p2 %in% c(i,j)), "se"],2), ")")
		}
	}
	}
colnames(plotdat2) = rownames(plotdat2) = gsub("Gran", "Granulocytes", rownames(plotdat2))
colnames(plotdat2) = rownames(plotdat2) = gsub("Age", "AgeAccel", rownames(plotdat2))
plotdat2[upper.tri(plotdat2)] = NA
plotdat2[plotdat2==1] = ""
plotdat2[is.na(plotdat2)] = ""


Heatmap(plotdat, 
	     row_names_gp = gpar(fontsize = 12),
	     column_names_gp = gpar(fontsize = 12),
	     na_col="white",
	     heatmap_legend_param = list(title = "rg"),
	     cluster_rows=FALSE, cluster_columns=FALSE, 
	    cell_fun = function(j, i, x, y, width, height, fill) {
        grid.text(sprintf("%s", plotdat2[i, j]), x, y, gp = gpar(fontsize = 10))})


# RG and MR plots
setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/")
require(gridExtra)
rg = read.delim("rg_plot_DM.txt")
rg$trait1 = gsub("Accel", "", rg$trait1)
rg$newtrait = c("Alcohol with meals", "BMI", 
	            "Current smoker", "Father's age at death", 
	            "Former drinker", "Long-standing illness",
	            "Mother's age at death", "Overall health rating*", 
	            "College/University degree", "Deprivation index", 
	            "Usual walking pace", "BMI", "Father's age at death", 
	            "Long-standing illness", "Overall health rating*", 
	            "College/University degree", "No recent physical activity")
rg$newtrait = factor(rg$newtrait, 
					levels = c("BMI", 							
						       "No recent physical activity", 
						       "Usual walking pace", 
						       "Alcohol with meals",
						       "Former drinker",
						       "Current smoker",
						       "Overall health rating*", 
						       "Long-standing illness",
						       "College/University degree",
						       "Deprivation index",
						       "Father's age at death",
						       "Mother's age at death"))

p1 = ggplot(rg[rg$trait1=="GrimAge",], aes(x=newtrait, y=rg)) + 
geom_point() + 
geom_errorbar(ymax=rg[rg$trait1=="GrimAge","UCI"], 
	          ymin=rg[rg$trait1=="GrimAge", "LCI"], width=0) + 
# facet_wrap(trait1~., scales="free_y") + 
ylim(-1,1) +
xlab("GWAS Trait") + 
ggtitle("GrimAge") + 
theme(plot.title = element_text(hjust = 0.5)) +
ylab("Genetic Correlation") + 
geom_hline(yintercept=0) +
coord_flip() 

p2 = ggplot(rg[rg$trait1=="PhenoAge",], aes(x=newtrait, y=rg)) + 
geom_point() + 
geom_errorbar(ymax=rg[rg$trait1=="PhenoAge","UCI"], 
	          ymin=rg[rg$trait1=="PhenoAge", "LCI"], width=0) + 
# facet_wrap(trait1~., scales="free_y") + 
ylim(-1,1) +
ggtitle("PhenoAge") + 
xlab("GWAS Trait") + 
ylab("Genetic Correlation") + 
theme(plot.title = element_text(hjust = 0.5)) +
geom_hline(yintercept=0) +
coord_flip() 

pdf(file="Plots/rg_plot.pdf")
print(grid.arrange(p1, p2))
dev.off()


# MR plot
mr = read.delim("MR_plot_DM.txt")
mr$exposure2 = c("BMI", "Waist circumference", 
	             "Hip circumference", "Current smoker", 
	             "College/University degree")
pdf("Plots/mr_plot.pdf")
ggplot(mr, aes(x=exposure2, y=b)) + 
geom_point() + 
geom_errorbar(ymax=mr$UCI, 
	          ymin=mr$LCI, width=0) + 
# facet_wrap(trait1~., scales="free_y") + 
ggtitle("GrimAge (outcome)") + 
theme(plot.title = element_text(hjust = 0.5)) +
xlab("UK Biobank Trait (exposure)") + 
ylab("Effect") + 
ylim(-5,5) + 
geom_hline(yintercept=0) +
coord_flip() 
dev.off()




######## Figure 3 (2 panels) ########
source("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Polygenic/pgrs_r2.r")
require(ggplot2)
require(reshape2)
require(RColorBrewer)
require(dplyr)

 plot_36 = melt(out36, id="thresh")
 plot_36$Cohort = "LBC36"
 plot_21 = melt(out21, id="thresh")
 plot_21$Cohort = "LBC21"

unique(plot_21$variable)
# [1] ieaa_r2   hannum_r2 pheno_r2  grim_r2   gran_r2   pai_r2

# YFS Data from Pashu (email 02/04/2020)
grim_r2  <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("grim_r2",6), value= c(0.01,0.41,0.32,0.12,0.21,0.17), Cohort=rep("YFS", 6))
gran_r2 <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("gran_r2",6), value= c(0.61,0.19,0.35,0.46,0.59,0.56), Cohort=rep("YFS", 6))
ieaa_r2 <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("ieaa_r2",6), value= c(3.37,1.17,0.71,0.43,0.23,0.22), Cohort=rep("YFS", 6))
pai1_r2 <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("pai1_r2",6), value= c(0.67,0.31,0.22,0.30,0.11,0.09), Cohort=rep("YFS", 6))
pheno_r2 <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("pheno_r2",6), value= c(0.34,0.44,0.56,0.50,0.12,0.11), Cohort=rep("YFS", 6))
hannum_r2 <- data.frame(thresh=plot_21$thresh[1:6], variable = rep("hannum_r2",6), value= c(0.61,0.18,0.01,0.02,0.01,0.01), Cohort=rep("YFS", 6))

plot_yfs = rbind(grim_r2, gran_r2, ieaa_r2, pai1_r2, pheno_r2, hannum_r2)

fhs = read.csv("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Polygenic/FHS_PGS.csv")
names(fhs) = c("X", "P<5e8", "P<0.01", "P<0.05", "P<0.1", "P<0.5", "P<1")
fhs = melt(fhs)
fhs$Cohort = "FHS"

fhs$X = gsub("GrimAge", "grim_r2", fhs$X)
fhs$X = gsub("Gran", "gran_r2", fhs$X)
fhs$X = gsub("IEAA", "ieaa_r2", fhs$X)
fhs$X = gsub("Hannum", "hannum_r2", fhs$X)
fhs$X = gsub("PAI1", "pai1_r2", fhs$X)
fhs$X = gsub("PhenoAge", "pheno_r2", fhs$X)

names(fhs) = c("variable", "thresh", "value", "Cohort")
fhs = fhs[,c("thresh", "variable", "value", "Cohort")]

bristol = read.table("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/Polygenic/results_prs_aries_bib_sabre.txt", header=T)
bristol = melt(bristol)
names(bristol)[2] = "thresh"
bristol = bristol[,c("thresh", "variable", "value", "Cohort")]
bristol$Cohort = gsub("Born_in_Bradford", "BiB", bristol$Cohort)

 plotdata2 = rbind(plot_36, plot_21, plot_yfs, fhs, bristol)
 plotdata2$Cohort = factor(plotdata2$Cohort, levels=c("LBC21", "LBC36", "YFS", "FHS", "ARIES", "BiB", "SABRE"))
 plotdata2$value = as.numeric(plotdata2$value)
 plotdata2$variable = gsub("_r2", "", plotdata2$variable)
 plotdata2$variable = gsub("gran|Gran", "Granulocytes", plotdata2$variable)
 plotdata2$variable = gsub("grim|GrimAge", "Grim AgeAccel", plotdata2$variable)
 plotdata2$variable = gsub("pheno|PhenoAge", "Pheno AgeAccel", plotdata2$variable)
 plotdata2$variable = gsub("ieaa", "IEAA", plotdata2$variable)
 plotdata2$variable = gsub("hannum|Hannum", "Hannum AgeAccel", plotdata2$variable)
 plotdata2$variable = gsub("pai|pai1", "PAI1", plotdata2$variable)
 colours = brewer.pal(9, "Set1")[1:7]
 company_colors <-c("#E50000", "#008A8A", "#AF0076", "#E56800", "#1717A0", "#E5AC00", "#00B700")
 plotdata2$thresh = gsub("P<5e8", "P<5e-8", plotdata2$thresh)
 plotdata2$thresh[is.na(plotdata2$value)] = "P<5e-8"
 plotdata2$thresh = factor(plotdata2$thresh, levels = c("P<5e-8", "P<0.01", "P<0.05", "P<0.1", "P<0.5", "P<1"))
#  tiff("Plots/PGRS_Percent_variance_explained_April2020.tiff",res=300, width=1800, height=1800)
 p1 = ggplot(data=plotdata2, aes(fill=Cohort, y=value, x=thresh)) +
  geom_boxplot(aes(group=thresh), outlier.shape=NA, show_guide=FALSE) +
  # geom_point(position=position_jitterdodge(), size=2, aes(colour=Cohort)) +
 facet_wrap(.~variable) + 
 scale_y_continuous(expand = c(0, 0), limits = c(0, 5)) +
 theme(axis.text.x = element_text(angle = 45, vjust=0.5)) +
 xlab("P-Value Threshold") + 
 ylab("% Variance Explained")  + 
 scale_colour_manual(values=company_colors)
 # dev.off()

data = read.xls("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/PRSAtlas/prsatlas_stacked.xlsx", header=T)
clocks = c("Gran.p1", "GrimAge.p1","Hannum", "IEAA", "PAI1", "PhenoAge")
data$PFDR = NA
for(clock in clocks){
	data[data$PRS == clock, "PFDR"] = p.adjust(data[data$PRS==clock, "P"], "fdr")
}


plot_phenos = c("Mother's age at death", 
	            "Father's age at death", 
	            "Townsend deprivation index at recruitment",
	            "Qualifications: College or University degree",
	            "Long-standing illness  disability or infirmity",
	            "Overall health rating",
	            "Current tobacco smoking", 
	            "Former alcohol drinker",
	            "Alcohol usually taken with meals",
	            "Usual walking pace", 
	            "Body mass index (BMI)")

plotdat = data[data$PRS == "GrimAge.p1" & data$Outcome %in% plot_phenos, ]
plotdat$Outcome = gsub("Townsend deprivation index at recruitment", "Deprivation index", plotdat$Outcome)
plotdat$Outcome = gsub("Qualifications: College or University degree", "College/University degree", plotdat$Outcome)
plotdat$Outcome = gsub("Long\\-standing illness  disability or infirmity", "Long-standing illness", plotdat$Outcome)
plotdat$Outcome = gsub("Current tobacco smoking", "Current smoker", plotdat$Outcome)
plotdat$Outcome = gsub("Former alcohol drinker", "Former drinker", plotdat$Outcome)
plotdat$Outcome = gsub("Alcohol usually taken with meals", "Alcohol with meals", plotdat$Outcome)
plotdat$Outcome = gsub("Body mass index \\(BMI\\)", "BMI", plotdat$Outcome)

plotdat$Outcome = factor(plotdat$Outcome, levels=rev(c("Mother's age at death",
	 												 "Father's age at death",
	 												 "Deprivation index",
	 												 "College/University degree",
	 												 "Long-standing illness",
	 												 "Overall health rating",
	 												 "Current smoker",
	 												 "Former drinker",
	 												 "Alcohol with meals",
	 												 "Usual walking pace",
	 												 "BMI")))

plotdat$upper = plotdat$Beta+plotdat$se
plotdat$lower = plotdat$Beta-plotdat$se
p2 = ggplot(plotdat, aes(x=Outcome, y=Beta)) + 
geom_point() + 
geom_errorbar(ymax=plotdat$upper, 
	          ymin=plotdat$lower, width=0) + 
# facet_wrap(trait1~., scales="free_y") + 
ggtitle("GrimAge PRSAtlas Associations (P<1)") + 
theme(plot.title = element_text(hjust = 0.5)) +
xlab("Outcome") + 
ylab("Effect") + 
ylim(-0.1,0.1) + 
geom_hline(yintercept=0) +
coord_flip() 



require(gridExtra)
grid.arrange(p1, p2, nrow=2)
