# MRMEGA File Prep
setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/MRMEGA")
require(data.table)
clocks = c("Gran", "GrimAge", "Hannum", "IEAA", "PAI1", "PhenoAge")

mega_out = hisp =  list()
for(clock in clocks){
	mega_out[[clock]] = fread(paste0(clock, "_MRMEGA_output_filtered"), header=T, stringsAsFactors=F)
	hisp[[clock]] = fread(paste0("../", clock, "/AA/CLEANED.MESA_HISP_", clock), header=T, stringsAsFactors=F)
}

mega_sig = mega_out 
plotdata = int = list()
for(clock in clocks){
	mega_sig[[clock]] = mega_out[[clock]][which(mega_out[[clock]]$"P-value_association" < 5e-8),]
	int = intersect(mega_sig[[clock]]$MarkerName, hisp[[clock]]$cptid)
	mega_sig[[clock]] = mega_sig[[clock]][which(mega_sig[[clock]]$MarkerName %in% int), ]
    plotdata[[clock]] = merge(data.frame(mega_sig[[clock]]), data.frame(hisp[[clock]]), by.x="MarkerName", by.y="cptid")
    flip = which(plotdata[[clock]]$EA == plotdata[[clock]]$OTHER_ALLELE & plotdata[[clock]]$NEA == plotdata[[clock]]$EFFECT_ALLELE)
    plotdata[[clock]]$EFFECT2 = plotdata[[clock]]$BETA
    plotdata[[clock]]$EFFECT2[flip] = plotdata[[clock]]$BETA[flip] * -1
    }





pdf("MESA_HISP_effects_vs_MRMEGA_effects", onefile=T)
par(mfrow=c(2,3))
for(i in clocks){
	plot(plotdata[[i]]$beta_0, plotdata[[i]]$EFFECT2, ylab="MRMEGA Effect (Beta 0)", xlab="MESA_HISP Effect", main=i, pch=19)
	abline(0,1)
}
par(mfrow=c(2,3))
for(i in clocks){
	plot(plotdata[[i]]$beta_1, plotdata[[i]]$EFFECT2, ylab="MRMEGA Effect (Beta 1)", xlab="MESA_HISP Effect", main=i, pch=19)
	abline(0,1)
}
par(mfrow=c(2,3))
for(i in clocks){
	plot(plotdata[[i]]$beta_2, plotdata[[i]]$EFFECT2, ylab="MRMEGA Effect (Beta 2)", xlab="MESA_HISP Effect", main=i, pch=19)
	abline(0,1)
}
par(mfrow=c(2,3))
for(i in clocks){
	plot(plotdata[[i]]$beta_3, plotdata[[i]]$EFFECT2, ylab="MRMEGA Effect (Beta 3)", xlab="MESA_HISP Effect", main=i, pch=19)
	abline(0,1)
}
dev.off()

pdf("MESA_HISP_effects_vs_MRMEGA_effects_beta0", onefile=T)
par(mfrow=c(2,3))
for(i in clocks){
	plot(plotdata[[i]]$beta_0, plotdata[[i]]$EFFECT2, ylab="MRMEGA Effect (Beta 0)", xlab="MESA_HISP Effect", main=i, pch=19)
	abline(0,1)
}
dev.off()


fuma = mega_out 
# Prep for FUMA
map_ea1 = fread("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/HRC_1000G_maps/HRC.r1-1.GRCh37.wgs.mac5.sites.tab.rsid_map", sep='\t', header=T, stringsAsFactors=FALSE)
map_ea2 = fread("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/HRC_1000G_maps/1000GP_p3v5_legends_rbind.noDup.noMono.noCnv.noCnAll.afref.EUR.txt", sep='\t', header=T, stringsAsFactors=FALSE)

map_ea1 = as.data.frame(map_ea1)
map_ea2 = as.data.frame(map_ea2)

for(clock in clocks){
fuma[[clock]] = as.data.frame(fuma[[clock]])
fuma[[clock]]$rsid_1000G = map_ea2[match(fuma[[clock]]$MarkerName, map_ea2$cptid), "cptid"]
map_ea1$rsid2 = paste0(map_ea1$chr, ":", map_ea1$pos)
fuma[[clock]]$hrc_rsid = map_ea1[match(fuma[[clock]]$MarkerName, map_ea1$rsid2), "rsid"]
fuma[[clock]]$SNP = fuma[[clock]]$hrc_rsid
fuma[[clock]][is.na(fuma[[clock]]$SNP), "SNP"] = fuma[[clock]]$rsid_1000G[is.na(fuma[[clock]]$SNP)]
fuma[[clock]] = fuma[[clock]][,c("SNP", "P-value_association", "EA", "NEA", "beta_0", "se_0", "Nsample")]

 names(fuma[[clock]]) = c("SNP", "P.value", "A1", "A2", "Effect", "StdErr", "TotalSampleSize")
 fwrite(fuma[[clock]], file=paste0(clock, "MRMEGA_FUMA_30032020.txt"), sep='\t', quote=F, row.names=F)
}




# Use FUMA leadsnps:
# MRMEGA File Prep
setwd("/Cluster_Filespace/Marioni_Group/Daniel/GWAS_AgeAccel/Meta_Analysis_Daniel/MRMEGA")
require(data.table)
clocks = c("Gran", "GrimAge", "Hannum", "IEAA", "PAI1", "PhenoAge")

hisp = mega_out = list()
for(clock in clocks){
	mega_out[[clock]] = fread(paste0(clock, "_MRMEGA_output_filtered"), header=T, stringsAsFactors=F)
	hisp[[clock]] = fread(paste0("../", clock, "/AA/CLEANED.MESA_HISP_", clock), header=T, stringsAsFactors=F)
}

fumafiles = dir()[grep("FUMA_job", dir())]
leadsnps = indsigsnps = plotdata = int = riskloci = list()
mega_sig = mega_out 
for(clock in clocks){
	riskloci[[clock]] = read.table(paste0(fumafiles[grep(clock, fumafiles)], "/GenomicRiskLoci.txt"), header=T, stringsAsFactors=F)
	leadsnps[[clock]] = read.table(paste0(fumafiles[grep(clock, fumafiles)], "/leadSNPs.txt"), header=T, stringsAsFactors=F)
	leadsnps[[clock]]$MarkerName = paste0(leadsnps[[clock]]$chr, ":", leadsnps[[clock]]$pos)
	indsigsnps[[clock]] = read.table(paste0(fumafiles[grep(clock, fumafiles)], "/IndSigSNPs.txt"), header=T, stringsAsFactors=F)
	indsigsnps[[clock]]$MarkerName = paste0(indsigsnps[[clock]]$chr, ":", indsigsnps[[clock]]$pos)
	mega_sig[[clock]] = data.frame(mega_out[[clock]])[which(mega_out[[clock]]$"P-value_association" < 5e-8),]
#	int = intersect(mega_sig[[clock]]$MarkerName, hisp[[clock]]$cptid)
#	mega_sig[[clock]] = mega_sig[[clock]][which(mega_sig[[clock]]$MarkerName %in% int), ]
    plotdata[[clock]] = merge(data.frame(mega_sig[[clock]]), data.frame(hisp[[clock]]), by.x="MarkerName", by.y="cptid", all.x=TRUE)
    flip = which(plotdata[[clock]]$EA == plotdata[[clock]]$OTHER_ALLELE & plotdata[[clock]]$NEA == plotdata[[clock]]$EFFECT_ALLELE)
    plotdata[[clock]]$EFFECT2 = plotdata[[clock]]$BETA
    plotdata[[clock]]$EFFECT2[flip] = plotdata[[clock]]$BETA[flip] * -1
    plotdata[[clock]] = plotdata[[clock]][which(plotdata[[clock]]$MarkerName %in% leadsnps[[clock]]$MarkerName), ]
    plotdata[[clock]]$Colour = ifelse(plotdata[[clock]]$PVAL < (0.05/sum(unlist(lapply(leadsnps, nrow)))), "red", "black")
    }

for(clock in clocks){
riskloci[[clock]]$MarkerName = paste0(riskloci[[clock]]$chr, ":", riskloci[[clock]]$pos)
}

# Assess overlap with independent loci identified in single ancestry analysis
require(GenomicRanges)
sig_eur = read.table("../Tables/EUR_Hits.txt", header=T, stringsAsFactors=F)
sig_eur$start=sig_eur$bp
sig_eur$end=sig_eur$bp
sig_afr = read.table("../Tables/AFR_Hits.txt", header=T, stringsAsFactors=F)
sig_afr$start=sig_afr$bp
sig_afr$end=sig_afr$bp
names(sig_eur) = gsub("Chr","chr",names(sig_eur))
names(sig_afr) = gsub("Chr","chr",names(sig_afr))
sig_eur_range = sig_afr_range = list()
for(clock in clocks){
sig_eur_range[[clock]] = makeGRangesFromDataFrame(sig_eur[sig_eur$Trait==clock,c("chr","start","end")])
sig_afr_range[[clock]] = makeGRangesFromDataFrame(sig_afr[sig_afr$Trait==clock,c("chr","start","end")])
}

riskloci2 = riskloci
for(clock in clocks){
# riskloci2[[clock]] = riskloci2[[clock]][,c("chr","start", "end")]
riskloci2[[clock]]$EUR = "N"
riskloci2[[clock]]$AFR = "N"
riskloci2[[clock]]$Novel = "N"
eur_ind = data.frame(findOverlaps(makeGRangesFromDataFrame(sig_eur[sig_eur$Trait==clock,]),  makeGRangesFromDataFrame(riskloci[[clock]])))[,2]
afr_ind = data.frame(findOverlaps(makeGRangesFromDataFrame(sig_afr[sig_afr$Trait==clock,]),  makeGRangesFromDataFrame(riskloci[[clock]])))[,2]
riskloci2[[clock]]$EUR[eur_ind] = "Y"
riskloci2[[clock]]$AFR[afr_ind] = "Y"
riskloci2[[clock]]$Novel[which(riskloci2[[clock]]$EUR == "N" & riskloci2[[clock]]$AFR == "N")] = "Y"
riskloci2[[clock]]$Trait = clock
}

write.table(do.call("rbind", riskloci2), file="../Tables/TransAncestryLoci_Overlap_Novel.txt", sep='\t', quote=F, row.names=F)

plotdata2 = plotdata
# Plot leadsnps effects from mrmega against mesa_hisp
pdf("MESA_HISP_effects_vs_MRMEGA_LeadSNP_effects.pdf", onefile=T)
par(mfrow=c(2,3))
for(i in clocks){
	ylim = c(min(plotdata[[i]]$EFFECT2, na.rm=T) - 0.1, max(plotdata[[i]]$EFFECT2, na.rm=T) + 0.1)
	xlim = c(min(plotdata[[i]]$beta_0) - 0.1, max(plotdata[[i]]$beta_0 + 0.1))
	lims = c(min(c(ylim, xlim)), max(c(ylim, xlim)))
	plot(plotdata[[i]]$beta_0, plotdata[[i]]$EFFECT2, 
		  ylab="MRMEGA Effect (Beta 0)", xlab="MESA_HISP Effect", 
		  main=i, pch=19, col = plotdata[[i]]$Colour,
		  xlim=lims, ylim=lims)
	abline(0,1,h=0, v=0)
}
dev.off()

# Plot lead snps from risk loci effects from mrmega against mesa_hisp (N=69)
plotdata3= plotdata
for(clock in clocks){
	plotdata3[[clock]] = plotdata3[[clock]][which(plotdata3[[clock]]$MarkerName %in% riskloci[[clock]]$MarkerName), ]
}
	pdf("MESA_HISP_effects_vs_MRMEGA_Riskloci_69LeadSNPs_effects.pdf", onefile=T)
par(mfrow=c(2,3))
for(i in clocks){
	ylim = c(min(plotdata3[[i]]$EFFECT2, na.rm=T) - 0.1, max(plotdata3[[i]]$EFFECT2, na.rm=T) + 0.1)
	xlim = c(min(plotdata3[[i]]$beta_0) - 0.1, max(plotdata3[[i]]$beta_0) + 0.1)
	if(i == "Gran"){
	ylim = c(min(plotdata3[[i]]$EFFECT2, na.rm=T) - 0.001, max(plotdata3[[i]]$EFFECT2, na.rm=T) + 0.001)
	xlim = c(min(plotdata3[[i]]$beta_0) - 0.001, max(plotdata3[[i]]$beta_0) + 0.001)

	}
	lims = c(min(c(ylim, xlim)), max(c(ylim, xlim)))
	plot(plotdata3[[i]]$beta_0, plotdata3[[i]]$EFFECT2, 
		  ylab="MRMEGA Effect (Beta 0)", xlab="MESA_HISP Effect", 
		  main=i, pch=19, col = plotdata3[[i]]$Colour, xlim=lims, ylim=lims)
	abline(0,1,h=0, v=0)
}
dev.off()

lapply(plotdata3, function(x){cor(x$beta_0, x$EFFECT2, use="pairwise.complete.obs")})

# prepare table
lead_table = do.call("rbind", leadsnps)
lead_table$Trait = gsub("\\..*", "", rownames(lead_table))
riskloci2 = do.call("rbind", riskloci)
riskloci2$Trait = gsub("\\..*", "", rownames(riskloci2))
lead_table$trait_locus = paste0(lead_table$Trait, "_", lead_table$GenomicLocus)
riskloci2$trait_locus = paste0(riskloci2$Trait, "_", riskloci2$GenomicLocus)
riskloci2$MarkerName = paste0(riskloci2$chr, ":", riskloci2$pos)
riskloci2$risklocus = paste0(riskloci2$chr, ":", riskloci2$start, "-", riskloci2$end)
plotdata2 = do.call("rbind", plotdata)
lead_table$risklocus = riskloci2[match(lead_table$trait_locus, riskloci2$trait_locus), "risklocus"]
lead_table$b = plotdata2[match(lead_table$MarkerName, plotdata2$MarkerName), "beta_0"]
lead_table$se = plotdata2[match(lead_table$MarkerName, plotdata2$MarkerName), "se_0"]
lead_table$p = plotdata2[match(lead_table$MarkerName, plotdata2$MarkerName), "P.value_association"]
lead_table$b_hisp = plotdata2[match(lead_table$MarkerName, plotdata2$MarkerName), "EFFECT2"]
lead_table$se_hisp = plotdata2[match(lead_table$MarkerName, plotdata2$MarkerName), "SE"]
lead_table$p_hisp = plotdata2[match(lead_table$MarkerName, plotdata2$MarkerName), "PVAL"]
out_table = lead_table[,c("risklocus", "rsID", "chr", "pos", "b", "se", "p", "b_hisp", "se_hisp", "p_hisp", "Trait")]
write.table(out_table, file="../Tables/MESA_HISP_lookup.txt", sep='\t', quote=F, row.names=F)


meta_results_eur = meta_results_afr = list()
for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){
	meta_results_eur[[i]] = fread(paste0("../", i, "/", i, "_EA_METAL_HET1.txt"), header=T, stringsAsFactors=F)
	meta_results_afr[[i]] = fread(paste0("../", i, "/", i, "_AA_METAL_HET1.txt"), header=T, stringsAsFactors=F)
}


#########################################
riskloci2 = riskloci
for(clock in clocks){
# riskloci2[[clock]] = riskloci2[[clock]][,c("chr","start", "end")]
riskloci2[[clock]]$EUR = "N"
riskloci2[[clock]]$AFR = "N"
riskloci2[[clock]]$Novel = "N"
eur_ind = data.frame(findOverlaps(makeGRangesFromDataFrame(sig_eur[sig_eur$Trait==clock,]),  makeGRangesFromDataFrame(riskloci[[clock]])))[,2]
afr_ind = data.frame(findOverlaps(makeGRangesFromDataFrame(sig_afr[sig_afr$Trait==clock,]),  makeGRangesFromDataFrame(riskloci[[clock]])))[,2]
riskloci2[[clock]]$EUR[eur_ind] = "Y"
riskloci2[[clock]]$AFR[afr_ind] = "Y"
riskloci2[[clock]]$Novel[which(riskloci2[[clock]]$EUR == "N" & riskloci2[[clock]]$AFR == "N")] = "Y"
riskloci2[[clock]]$Trait = clock
}
# x1 = do.call("rbind", riskloci2)
x1 = do.call("rbind", riskloci2)
x1 = x1[which(x1$Novel == "Y"), ]
x1$MarkerName = paste0(x1$chr, ":", x1$pos)

pdf("../Plots/MRMEGA_Freqs_vs_EUR_Freqs.pdf")
par(mfrow=c(2,3))
	for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){
		tmp_x1 = x1[x1$Trait==i, "MarkerName"]
		tmp_x = mega_sig[[i]][which(mega_sig[[i]]$MarkerName %in% tmp_x1),] 
		tmp_y = meta_results_eur[[i]][match(tmp_x$MarkerName, meta_results_eur[[i]]$MarkerName),]
		tmp_y$Allele1 = toupper(tmp_y$Allele1)
		tmp_y$Allele2 = toupper(tmp_y$Allele2)
		stopifnot(isTRUE(all.equal(tmp_x$MarkerName, tmp_y$MarkerName)))
		tmp_y$Freq_new = tmp_y$Freq1
		tmp_y[tmp_y$Allele2 == tmp_x$EA,"Freq_new"] = 1 - tmp_y[tmp_y$Allele2 == tmp_x$EA,"Freq1"]
		plot(tmp_x$EAF, tmp_y$Freq_new, main=paste0(i, "\nMRMEGA EAF vs EUR EAF"), xlab = "MRMEGA_EAF", ylab="EUR_EAF")
		abline(0,1)
}
dev.off()


pdf("../Plots/MRMEGA_Freqs_vs_AFR_Freqs.pdf")
par(mfrow=c(2,3))
	for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){
		tmp_x1 = x1[x1$Trait==i, "MarkerName"]
		tmp_x = mega_sig[[i]][which(mega_sig[[i]]$MarkerName %in% tmp_x1),] 
		tmp_y = meta_results_afr[[i]][match(tmp_x$MarkerName, meta_results_afr[[i]]$MarkerName),]
		tmp_y$Allele1 = toupper(tmp_y$Allele1)
		tmp_y$Allele2 = toupper(tmp_y$Allele2)
		stopifnot(isTRUE(all.equal(tmp_x$MarkerName, tmp_y$MarkerName)))
		tmp_y$Freq_new = tmp_y$Freq1
		tmp_y[tmp_y$Allele2 == tmp_x$EA,"Freq_new"] = 1 - tmp_y[tmp_y$Allele2 == tmp_x$EA,"Freq1"]
		plot(tmp_x$EAF, tmp_y$Freq_new, main=paste0(i, "\nMRMEGA EAF vs AFR EAF"), xlab = "MRMEGA_EAF", ylab="AFR_EAF")
		abline(0,1)
}
dev.off()


pdf("../Plots/MREGA_SNPS_EUR_Freqs_vs_AFR_Freqs.pdf")
par(mfrow=c(2,3))
	for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){
		tmp_x1 = x1[x1$Trait==i, ]
		tmp_x = meta_results_eur[[i]][which(meta_results_eur[[i]]$MarkerName %in% tmp_x1$MarkerName),] 
		tmp_y = meta_results_afr[[i]][which(meta_results_afr[[i]]$MarkerName %in% tmp_x1$MarkerName),]
		tmp_y = tmp_y[match(tmp_x$MarkerName, tmp_y$MarkerName), ]
		tmp_x$Allele1 = toupper(tmp_x$Allele1)
		tmp_x$Allele2 = toupper(tmp_x$Allele2)
		tmp_y$Allele1 = toupper(tmp_y$Allele1)
		tmp_y$Allele2 = toupper(tmp_y$Allele2)
		stopifnot(isTRUE(all.equal(tmp_x$MarkerName, tmp_y$MarkerName)))
		tmp_x2 = mega_out[[i]][match(tmp_x$MarkerName, mega_out[[i]]$MarkerName), ]
		tmp_x$Freq_new = tmp_x$Freq1
		tmp_y$Freq_new = tmp_y$Freq1
		tmp_x[tmp_x$Allele2 == tmp_x2$EA, "Freq_new"] = 1 - tmp_x[tmp_x$Allele2 == tmp_x2$EA, "Freq1"]
		tmp_y[tmp_y$Allele2 == tmp_x2$EA, "Freq_new"] = 1 - tmp_y[tmp_y$Allele2 == tmp_x2$EA, "Freq1"]
		plot(tmp_x$Freq_new, tmp_y$Freq_new, main=paste0(i, "\nEUR EAF vs AFR EAF"), xlab = "EUR_EAF", ylab="AFR_EAF")
		abline(0,1)
}
dev.off()


######################################################################################
################ Same as above but effects instead of AF #############################
######################################################################################
pdf("../Plots/MRMEGA_Effects_vs_EUR_Effects.pdf")
par(mfrow=c(2,3))
	for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){
		tmp_x1 = x1[x1$Trait==i, "MarkerName"]
		tmp_x = mega_sig[[i]][which(mega_sig[[i]]$MarkerName %in% tmp_x1),] 
		tmp_y = meta_results_eur[[i]][match(tmp_x$MarkerName, meta_results_eur[[i]]$MarkerName),]
		tmp_y$Allele1 = toupper(tmp_y$Allele1)
		tmp_y$Allele2 = toupper(tmp_y$Allele2)
		stopifnot(isTRUE(all.equal(tmp_x$MarkerName, tmp_y$MarkerName)))
		tmp_y$Effect_new = tmp_y$Effect
		tmp_y[tmp_y$Allele2 == tmp_x$EA,"Effect_new"] = -1*tmp_y[tmp_y$Allele2 == tmp_x$EA,"Effect"]
		plot(tmp_x$beta_0, tmp_y$Effect_new, main=paste0(i, "\nMRMEGA Effect vs EUR Effect"), xlab = "MRMEGA_Effect", ylab="EUR_Effect")
		abline(0,1)
}
dev.off()


pdf("../Plots/MRMEGA_Effects_vs_AFR_Effects.pdf")
par(mfrow=c(2,3))
	for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){
		tmp_x1 = x1[x1$Trait==i, "MarkerName"]
		tmp_x = mega_sig[[i]][which(mega_sig[[i]]$MarkerName %in% tmp_x1),] 
		tmp_y = meta_results_afr[[i]][match(tmp_x$MarkerName, meta_results_afr[[i]]$MarkerName),]
		tmp_y$Allele1 = toupper(tmp_y$Allele1)
		tmp_y$Allele2 = toupper(tmp_y$Allele2)
		stopifnot(isTRUE(all.equal(tmp_x$MarkerName, tmp_y$MarkerName)))
		tmp_y$Effect_new = tmp_y$Effect
		tmp_y[tmp_y$Allele2 == tmp_x$EA,"Effect_new"] = -1*tmp_y[tmp_y$Allele2 == tmp_x$EA,"Effect"]
		plot(tmp_x$beta_0, tmp_y$Effect_new, main=paste0(i, "\nMRMEGA Effect vs AFR Effect"), xlab = "MRMEGA_Effect", ylab="AFR_Effect")
		abline(0,1)
}
dev.off()


pdf("../Plots/MREGA_SNPS_EUR_Effects_vs_AFR_Effects.pdf")
par(mfrow=c(2,3))
	for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){
		tmp_x1 = x1[x1$Trait==i, ]
		tmp_x = meta_results_eur[[i]][which(meta_results_eur[[i]]$MarkerName %in% tmp_x1$MarkerName),] 
		tmp_y = meta_results_afr[[i]][which(meta_results_afr[[i]]$MarkerName %in% tmp_x1$MarkerName),]
		tmp_y = tmp_y[match(tmp_x$MarkerName, tmp_y$MarkerName), ]
		tmp_x$Allele1 = toupper(tmp_x$Allele1)
		tmp_x$Allele2 = toupper(tmp_x$Allele2)
		tmp_y$Allele1 = toupper(tmp_y$Allele1)
		tmp_y$Allele2 = toupper(tmp_y$Allele2)
		stopifnot(isTRUE(all.equal(tmp_x$MarkerName, tmp_y$MarkerName)))
		tmp_x2 = mega_out[[i]][match(tmp_x$MarkerName, mega_out[[i]]$MarkerName), ]
		tmp_x$Effect_new = tmp_x$Effect
		tmp_y$Effect_new = tmp_y$Effect
		tmp_x[tmp_x$Allele2 == tmp_x2$EA, "Effect_new"] = -1*tmp_x[tmp_x$Allele2 == tmp_x2$EA, "Effect"]
		tmp_y[tmp_y$Allele2 == tmp_x2$EA, "Effect_new"] = -1*tmp_y[tmp_y$Allele2 == tmp_x2$EA, "Effect"]
		plot(tmp_x$Effect_new, tmp_y$Effect_new, main=paste0(i, "\nEUR Effect vs AFR Effect"), xlab = "EUR_Effect", ylab="AFR_Effect")
		abline(0,1)
}
dev.off()


######################################################################################
################ Plot AFR-only and EUR-only effects in MRMEGA output #################
######################################################################################

# Overlapping ranges from population-specific analyses
# Remove these indices from sig_eur/sig_afr 
sig_eur2 = list()
sig_afr2 = list()
for(clock in clocks){
eur_ind2 = data.frame(findOverlaps(makeGRangesFromDataFrame(riskloci[[clock]]), makeGRangesFromDataFrame(sig_eur[sig_eur$Trait==clock,])))[,2]
afr_ind2 = data.frame(findOverlaps(makeGRangesFromDataFrame(riskloci[[clock]]), makeGRangesFromDataFrame(sig_afr[sig_afr$Trait==clock,])))[,2]
sig_eur2[[clock]] = sig_eur[sig_eur$Trait==clock,]
sig_eur2[[clock]] = sig_eur2[[clock]][-eur_ind2,]
if(nrow(sig_eur2[[clock]]) > 0){
sig_eur2[[clock]]$MarkerName = paste0(sig_eur2[[clock]]$chr, ":", sig_eur2[[clock]]$bp)
}
sig_afr2[[clock]] = sig_afr[sig_afr$Trait==clock,]
sig_afr2[[clock]] = sig_afr2[[clock]][-afr_ind2,]
if(nrow(sig_afr2[[clock]]) > 0){
sig_afr2[[clock]]$MarkerName = paste0(sig_afr2[[clock]]$chr, ":", sig_afr2[[clock]]$bp)
}
}

# Cor between heterogeneous SNPs
sig_afr3 = lapply(sig_afr2, function(x){x[!is.na(x$p_eur), ]})
sig_eur3 = lapply(sig_eur2, function(x){x[!is.na(x$p_afr), ]})

lapply(sig_afr3, function(x){cor(x[,5],x[,8])})
lapply(sig_eur3, function(x){cor(x[,5],x[,8])})



#########################################################################
#########################################################################
###### Plot European-specific SNP's effects against MRMEGA result #######
#########################################################################
#########################################################################
df = list()
cors = list()
#pdf("../Plots/EUR_specific_SNP_effects_EUR_vs_AFR.pdf")
# par(mfrow=c(2,3))
	for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){ # 100% overlap between EUR Gran SNPs and MRMEGA
		tmp_x1 = sig_eur2[[i]]$MarkerName
		if(length(tmp_x1) > 0){
		tmp_x = meta_results_afr[[i]][which(meta_results_afr[[i]]$MarkerName %in% tmp_x1),] 
		tmp_y = meta_results_eur[[i]][match(tmp_x$MarkerName, meta_results_eur[[i]]$MarkerName),]
		if(nrow(tmp_x) > 0){
		tmp_y$Allele1 = toupper(tmp_y$Allele1)
		tmp_y$Allele2 = toupper(tmp_y$Allele2)
		
		stopifnot(isTRUE(all.equal(tmp_x$MarkerName, tmp_y$MarkerName)))
		tmp_y$Effect_new = tmp_y$Effect
		tmp_y[tmp_y$Allele2 == tmp_x$Allele1,"Effect_new"] = -1*tmp_y[tmp_y$Allele2 == tmp_x$EA,"Effect"]
			if(nrow(tmp_x)>=3){
			cors[[i]] = cor(tmp_x$Effect, tmp_y$Effect_new)
				}
		ylim = c(min(tmp_y$Effect_new, na.rm=T) - 0.1, max(tmp_y$Effect_new, na.rm=T) + 0.1)
		xlim = c(min(tmp_x$Effect, na.rm=T) - 0.1, max(tmp_x$Effect, na.rm=T) + 0.1)
		lims = c(min(c(ylim, xlim)), max(c(ylim, xlim)))
		df[[i]] = cbind(tmp_y[,c("MarkerName", "Effect_new", "Freq1", "P-value", "StdErr")],
			            tmp_x[,c("Effect", "Freq1", "P-value", "StdErr")])
		df[[i]]$Trait = i
		names(df[[i]])[2:5] = c("B_EUR", "Freq_EUR", "P_EUR", "SE_EUR")
		names(df[[i]])[6:9] = c("B_AFR", "Freq_AFR", "P_AFR", "SE_AFR")
	# 	plot(tmp_x$Effect, tmp_y$Effect_new, main=paste0(i, " EUR_SNPs \nAFR Effect vs EUR Effect"), 
	# 		 xlab = "AFR_Effect", ylab="EUR_Effect", 
	# 		 xlim=lims, ylim=lims)
	# 	abline(0,1,h=0, v=0)
	 }
}
}
# dev.off()

eur_snps = do.call("rbind", df)
write.table(eur_snps, file="EUR_specific_SNPs_AFR_effects_freqs.txt", sep='\t', row.names=F, quote=F)

#########################################################################
#########################################################################
###### Plot African-specific SNP's effects against MRMEGA result #######
#########################################################################
#########################################################################
df = list()
cors2 = list()
# pdf("../Plots/AFR_specific_SNP_effects_AFR_vs_EUR.pdf")
# par(mfrow=c(2,3))
	for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){ # 100% overlap between EUR Gran SNPs and MRMEGA
		tmp_x1 = sig_afr2[[i]]$MarkerName
		if(length(tmp_x1) > 0){
		tmp_x = meta_results_eur[[i]][which(meta_results_eur[[i]]$MarkerName %in% tmp_x1),] 
		tmp_y = meta_results_afr[[i]][match(tmp_x$MarkerName, meta_results_afr[[i]]$MarkerName),]
				if(nrow(tmp_x) > 0){
		tmp_y$Allele1 = toupper(tmp_y$Allele1)
		tmp_y$Allele2 = toupper(tmp_y$Allele2)
		
		stopifnot(isTRUE(all.equal(tmp_x$MarkerName, tmp_y$MarkerName)))
		tmp_y$Effect_new = tmp_y$Effect
		tmp_y[tmp_y$Allele2 == tmp_x$Allele1,"Effect_new"] = -1*tmp_y[tmp_y$Allele2 == tmp_x$EA,"Effect"]
			if(nrow(tmp_x)>=3){
			cors[[i]] = cor(tmp_x$Effect, tmp_y$Effect_new)
				}
		ylim = c(min(tmp_y$Effect_new, na.rm=T) - 0.1, max(tmp_y$Effect_new, na.rm=T) + 0.1)
		xlim = c(min(tmp_x$Effect, na.rm=T) - 0.1, max(tmp_x$Effect, na.rm=T) + 0.1)
		lims = c(min(c(ylim, xlim)), max(c(ylim, xlim)))
		df[[i]] = cbind(tmp_y[,c("MarkerName", "Effect_new", "Freq1", "P-value", "StdErr")],
			            tmp_x[,c("Effect", "Freq1", "P-value", "StdErr")])
		df[[i]]$Trait = i
		names(df[[i]])[2:5] = c("B_AFR", "Freq_AFR", "P_AFR", "SE_AFR")
		names(df[[i]])[6:9] = c("B_EUR", "Freq_EUR", "P_EUR", "SE_EUR")
	# 	plot(tmp_x$Effect, tmp_y$Effect_new, main=paste0(i, " EUR_SNPs \nAFR Effect vs EUR Effect"), 
	# 		 xlab = "AFR_Effect", ylab="EUR_Effect", 
	# 		 xlim=lims, ylim=lims)
	# 	abline(0,1,h=0, v=0)
	 }
}
}

afr_snps = do.call("rbind", df)
write.table(afr_snps, file="AFR_specific_SNPs_EUR_effects_freqs.txt", sep='\t', row.names=F, quote=F)


######### Correlation of SNPs with genetic heterogeneity ############
commonsnps = cors = list()
	for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){ # 100% overlap between EUR Gran SNPs and MRMEGA
		tmp_x1 = sig_eur2[[i]]$MarkerName
		if(length(tmp_x1) > 0){
		tmp_x = meta_results_afr[[i]][which(meta_results_afr[[i]]$MarkerName %in% tmp_x1),] 
		tmp_y = meta_results_eur[[i]][match(tmp_x$MarkerName, meta_results_eur[[i]]$MarkerName),]
		if(nrow(tmp_x) > 0){
		tmp_y$Allele1 = toupper(tmp_y$Allele1)
		tmp_y$Allele2 = toupper(tmp_y$Allele2)
		
		stopifnot(isTRUE(all.equal(tmp_x$MarkerName, tmp_y$MarkerName)))
		tmp_y$Effect_new = tmp_y$Effect
		tmp_y[tmp_y$Allele2 == tmp_x$Allele1,"Effect_new"] = -1*tmp_y[tmp_y$Allele2 == tmp_x$EA,"Effect"]
			if(nrow(tmp_x)>=3){
			cors[[i]] = cor(tmp_x$Effect, tmp_y$Effect_new)
				}
		ylim = c(min(tmp_y$Effect_new, na.rm=T) - 0.1, max(tmp_y$Effect_new, na.rm=T) + 0.1)
		xlim = c(min(tmp_x$Effect, na.rm=T) - 0.1, max(tmp_x$Effect, na.rm=T) + 0.1)
		lims = c(min(c(ylim, xlim)), max(c(ylim, xlim)))

		plot(tmp_x$Effect, tmp_y$Effect_new, main=paste0(i, " EUR_SNPs \nAFR Effect vs EUR Effect"), 
			 xlab = "AFR_Effect", ylab="EUR_Effect", 
			 xlim=lims, ylim=lims)
		abline(0,1,h=0, v=0)
	}
}
}
dev.off()

cors2 = list()
pdf("../Plots/AFR_specific_SNP_effects_AFR_vs_EUR.pdf")
par(mfrow=c(2,3))
	for(i in c("Gran", "GrimAge", "IEAA", "Hannum", "PhenoAge", "PAI1")){ # 100% overlap between EUR Gran SNPs and MRMEGA
		tmp_x1 = sig_afr2[[i]]$MarkerName
		if(length(tmp_x1) > 0){
		tmp_x = meta_results_eur[[i]][which(meta_results_eur[[i]]$MarkerName %in% tmp_x1),] 
		tmp_y = meta_results_afr[[i]][match(tmp_x$MarkerName, meta_results_afr[[i]]$MarkerName),]
		if(nrow(tmp_x) > 0){
		tmp_y$Allele1 = toupper(tmp_y$Allele1)
		tmp_y$Allele2 = toupper(tmp_y$Allele2)
		
		stopifnot(isTRUE(all.equal(tmp_x$MarkerName, tmp_y$MarkerName)))
		tmp_y$Effect_new = tmp_y$Effect
		tmp_y[tmp_y$Allele2 == tmp_x$Allele1,"Effect_new"] = -1*tmp_y[tmp_y$Allele2 == tmp_x$EA,"Effect"]
		
			if(nrow(tmp_x)>=3){
			cors2[[i]] = cor(tmp_x$Effect, tmp_y$Effect_new)
				}

		ylim = c(min(tmp_y$Effect_new, na.rm=T) - 0.1, max(tmp_y$Effect_new, na.rm=T) + 0.1)
		xlim = c(min(tmp_x$Effect, na.rm=T) - 0.1, max(tmp_x$Effect, na.rm=T) + 0.1)
		lims = c(min(c(ylim, xlim)), max(c(ylim, xlim)))

		plot(tmp_x$Effect, tmp_y$Effect_new, main=paste0(i, " AFR_SNPs \nEUR Effect vs AFR Effect"), 
			 xlab = "EUR_Effect", ylab="AFR_Effect", 
			 xlim=lims, ylim=lims)
		abline(0,1,h=0, v=0)
	}
}
}
dev.off()




